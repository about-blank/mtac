// Package cli contains the files for executing mtac from a commandline
package cli

import (
	"os"

	"github.com/spf13/cobra"

	"mtac/logging"
	"mtac/parser"

	"mtac/output"
)

var dryrunBool bool
var outputPath string
var configPath string
var loglevel string
var errorCheck bool = false

var rootCmd = &cobra.Command{
	Use:   "mtac",
	Short: "MTAC is a Security-Audit Checker",
	Long:  ` --- `,

	Run: func(cmd *cobra.Command, args []string) {
		logger := logging.SimpleLogger()
		if dryrunBool {
			logger.Info("Called dryrun.")
			parser.Dryrun = true
		}

		if loglevel != "" {
			res := logging.SetLogLevel(loglevel)
			if !res {
				logger.Errorf("Can't set loglevel '%s'.", loglevel)
			}
		}

		if outputPath != "" {

			//this checks if the path exists
			if _, err := os.Stat(outputPath); err == nil {

				//the path does exist
				output.OutputZipPathAndName = outputPath
			} else if os.IsNotExist(err) {
				//the path does not exist
				logger.Error("Output: Filepath invalid.")
				logger.Info("Output will be created in working directory")
			} else {
				//the path is weird
				logger.Error("Problem with path. ", err)
				errorCheck = true
			}
		}

		if configPath != "" {
			//this checks if the path exists
			if _, err := os.Stat(configPath); err == nil {
				// the path does exist
				parser.SetConf(configPath)
				logger.Infof("Searching for Cfg at: ./%s.", configPath)
			} else if os.IsNotExist(err) {
				// the path does not exist

				logger.Error(" could not close debug_temp for some reason. the temp directory will not be deleted.", err)
				logging.PanicLogger(err)

				//kinda pointless tbh but better safe than sorry
				errorCheck = true
			} else {
				//the path is weird

				logger.Error(" could not close debug_temp for some reason. the temp directory will not be deleted.", err)
				logging.PanicLogger(err)
				//kinda pointless tbh but better safe than sorry
				errorCheck = true
			}
		} else {
			parser.SetConf("")
		}

		if !errorCheck {
			parser.Parser()
		}

	},
}

// Execute executes the program.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

// init sets the logging and the variables for the shell.
func init() {
	rootCmd.Flags().BoolVarP(&dryrunBool, "dryrun", "d", false, "checks if the config is properly written")
	rootCmd.Flags().StringVarP(&configPath, "config", "c", "", "specifies the config-path")
	rootCmd.Flags().StringVarP(&outputPath, "output", "o", "", "specifies the output-path")
	rootCmd.Flags().StringVarP(&loglevel, "loglevel", "l", "", "sets loglevel (e.g. Info)")
	output.InitLog()
}
