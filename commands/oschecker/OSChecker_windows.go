package oschecker

import (
	"mtac/logging"
	"os/exec"
)

// getVersion checks which Windows version the user runs.
func GetVersion() string {
	logger := logging.SimpleLogger()
	o, err := exec.Command("ver", "").Output()
	if err != nil {
		logger.Error("Can't get Windows version. ", err)
	}
	return string(o)
}

// getOS is equivalent to GetOS on windows.
func getOS() string {
	return GetOS()
}
