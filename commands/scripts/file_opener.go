package scripts

import (
	"io/ioutil"
	"mtac/logging"

	"github.com/dop251/goja"
)

// type File is a struct for a new file.
type File struct{}

/*
	NewFile returns a new file-object upon being called.
*/
func NewFile(vm *goja.Runtime) *File {
	return &File{}
}

/*
	Open opens and returns a file as a string upon being called from inside
	the goja-runtime.
*/
func (f File) Open(target string) string {
	logger := logging.SimpleLogger()
	file, err := ioutil.ReadFile(target)
	if err != nil {
		logger.Println(err)
	}
	return string(file)
}
