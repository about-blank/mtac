package isInstalled

import (
	"mtac/commands"
	"mtac/commands/oschecker"
	"mtac/logging"
	"os/exec"
	"strings"
)

// Execute checks, wether the package given by the map is installed on the operating system.
func (i IsInstalled) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	// insert OS Name
	OsTemp := oschecker.GetVersion()

	// which Command checks, whether the package exists.
	whichCmd := exec.Command("which", i.packageName)

	// rpmCmd,aptCmd and pacmanCmd returns information about the given Packagename
	// Applied under Fedora etc.
	rpmCmd := exec.Command("rpm", "-q", i.packageName)

	// Applied under Debian, Ubuntu etc.
	aptCmd := exec.Command("dpkg-query", "--showformat='${Version}\n'", "--show", i.packageName)

	// Applied under Arch Linux
	pacmanCmd := exec.Command("pacman", "-Qo", i.packageName)

	if strings.EqualFold(OsTemp, "Fedora") || strings.EqualFold(OsTemp, "RHEL") || strings.EqualFold(OsTemp, "CentOS") || strings.EqualFold(OsTemp, "Scientific Linux") || strings.EqualFold(OsTemp, "SUSE Linux") || strings.EqualFold(OsTemp, "openSUSE") {
		whichOutput, err := whichCmd.Output()

		//Checks if whichOutput is empty or not
		if existsCheck(string(whichOutput), err) != "" {
			mainOutput, err := rpmCmd.Output()
			return mainCheck(i.packageName, string(whichOutput), names, string(mainOutput), err), true
		} else {
			logger.Warn("Empty filepath.")
			return emptyA(names, i.packageName, logger)
		}
	} else if strings.EqualFold(OsTemp, "Debian") || strings.EqualFold(OsTemp, "Ubuntu") || strings.EqualFold(OsTemp, "Mint Linux") || strings.EqualFold(OsTemp, "Kali Linux") {
		whichOutput, err := whichCmd.Output()

		if existsCheck(string(whichOutput), err) != "" {
			mainOutput, err := aptCmd.Output()
			return mainCheck(i.packageName, string(whichOutput), names, string(mainOutput), err), true
		} else {
			logger.Warn("Empty filepath.")
			return emptyA(names, i.packageName, logger)
		}
	} else if strings.EqualFold(OsTemp, "arch linux") {
		mainOutput, err := pacmanCmd.Output()

		//mainCheck func derivation for Arch LInux: archCheck
		return archCheck(i.packageName, names, string(mainOutput), err)
	} else {
		logger.Warnf("OS-Name '%s' doesn't match with the existing cases.", OsTemp)
		return emptyA(names, i.packageName, logger)
	}

}

// archCheck checks the installation for Arch Linux (has no which command).
func archCheck(pkgName string, names map[string]string, result string, err error) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	if err != nil {
		logger.Error("ERROR:", err)
		return emptyA(names, pkgName, logger)
	}

	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    true,
		Result:    "Path and Version of " + pkgName + ": " + result,
		Filepath:  "",
	}

	return a, true
}

// mainCheck collects all the infos to create the Artifact, that is given to the output.
func mainCheck(pkgName string, filepath string, names map[string]string, version string, err error) commands.Artifact {
	var logger = logging.CommandLogger(names)
	if err != nil {
		a := commands.Artifact{
			ModulName: names["name"],
			TaskName:  names["audit"],
			IsFile:    true,
			Result:    "Version could not be found for " + pkgName,
			Filepath:  filepath,
		}
		logger.Error("ERROR:", err)
		return a
	}

	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    true,
		Result:    "Result for " + pkgName + ": " + version,
		Filepath:  filepath,
	}

	return a
}
