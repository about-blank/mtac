// Package testMod contains the create, verify and execute functions for the command.
package testMod

import (
	"mtac/commands"
	"mtac/logging"
	"os/exec"
	"runtime"
	"strings"
)

// implements 'commander'

// Struct with multiple parameters, that can be used with the Create function.
type TestMod struct {
	cmdBase commands.Cmd
	module  string
}

// Create is the function, that initializes the struct and checks the syntax.
func (t TestMod) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := TestMod{cmdBase: cmd}
	if temp, ok := m["module"].(string); !ok {
		logger.Error("'module' misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Error(("'module' empty."))
		struc.module = temp
		return false, struc
	} else {
		struc.module = temp
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (t TestMod) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(t, m, logger)
}

// Execute the commands 'modprobe -n -v _module_' and 'lsmod | grep _module_'
func (t TestMod) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	checkMod := false
	checkLsmod := false
	checkEnd := false

	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}

	if !strings.EqualFold(runtime.GOOS, "linux") {
		logger.Errorf("TestMod not implemented for %s.", runtime.GOOS)
		return a, false
	}

	// exec.Command executes this command in the cmd of linux. the return value is a byte[]
	modprobe := exec.Command("modprobe", "-n", "-v", t.module)
	output, err := modprobe.CombinedOutput()
	if err != nil {
		logger.Error(err, ": ", string(output))
		return a, false
	} else {

		// output is a byte[] with string() it parses to string
		a.Result = modprobe.String() + ": \n"
		a.Result += string(output)
		checkMod = true
	}

	// exec.Command executes this command in the cmd of linux. the return value is a byte[]
	lsmod := exec.Command("bash", "-c", "lsmod | grep "+t.module)
	out, err := lsmod.CombinedOutput()
	if err != nil {
		logger.Errorf("Cant execute %s.", t.module)
		return a, false
	} else {

		// for the Artifact safe all given values
		a.Result += lsmod.String() + ": \n"
		if string(out) == "" {
			a.Result += "null"
		} else {
			a.Result += string(out)
		}
		checkLsmod = true
	}

	// if true the func give true else false
	if checkLsmod && checkMod {
		checkEnd = true
	}
	return a, checkEnd
}
