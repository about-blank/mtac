package isInstalled

import (
	"mtac/commands/oschecker"
	"mtac/commands/testfuncs"
	"strings"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"packageName": ""}, Res: false},
	{M: map[interface{}]interface{}{"packageName": "foo"}, Res: true},
	{M: map[interface{}]interface{}{"packageName": true}, Res: false},
}

func TestCreateTest(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, IsInstalled{})
}

func TestExecute_isInstalled_windows(t *testing.T) {
	if strings.EqualFold(oschecker.GetOS(), "Windows") {

		testBool := false
		names := map[string]string{
			"ModulName": "Modulname",
			"TaskName":  "Taskname",
		}

		//Tests only works on specific PCs
		testIs1 := IsInstalled{
			packageName: "Cisco Webex Meetings",
		}
		_, testBool = testIs1.Execute(names)

		if testBool != true {
			t.Error("Existing Package:", testIs1.packageName, "not found!")
		}

		testIs2 := IsInstalled{
			packageName: "Adobe Acrobat DC",
		}
		_, testBool = testIs2.Execute(names)

		if testBool != true {
			t.Error("Existing Package:", testIs2.packageName, "not found!")
		}

		testIs3 := IsInstalled{
			packageName: "Python Launcher",
		}
		_, testBool = testIs3.Execute(names)

		if testBool != true {
			t.Error("Existing Package:", testIs3.packageName, "not found!")
		}

		//Tests for unknown Software
		testIs4 := IsInstalled{
			packageName: "Blablablabla",
		}
		_, testBool = testIs4.Execute(names)

		if testBool == true {
			t.Error("I found the Software:", testIs4.packageName, "thats impossible...")
		}

		testIs5 := IsInstalled{
			packageName: "weoirfwjfwoiehfkosmfes",
		}
		_, testBool = testIs5.Execute(names)

		if testBool == true {
			t.Error("I found the Software:", testIs5.packageName, "thats impossible...")
		}
	}
}

func TestExecute_isInstalled_linux(t *testing.T) {
	OsTemp := oschecker.GetVersion()

	if OsTemp == "Ubuntu" {
		testBool := false
		names := map[string]string{
			"ModulName": "Modulname",
			"TaskName":  "Taskname",
		}

		//Tests for existing Software
		testIs1 := IsInstalled{
			packageName: "nano",
		}

		_, testBool = testIs1.Execute(names)
		if testBool == false {
			t.Error("Existing Package:", testIs1.packageName, "not found!")
		}

		testIs2 := IsInstalled{
			packageName: "bash",
		}

		_, testBool = testIs2.Execute(names)
		if testBool == false {
			t.Error("Existing Package:", testIs2.packageName, "not found!")
		}

		//Tests for unknown Software
		testIs3 := IsInstalled{
			packageName: "asdasd",
		}

		_, testBool = testIs3.Execute(names)
		if testBool == true {
			t.Error("I found the Software:", testIs3.packageName, "thats impossible...")
		}

		testIs4 := IsInstalled{
			packageName: "",
		}

		_, testBool = testIs4.Execute(names)
		if testBool == true {
			t.Error("I found the Software:", testIs4.packageName, "thats impossible...")
		}

	}

}
