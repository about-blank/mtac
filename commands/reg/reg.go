// Package reg contains the create, verify and execute functions for the command.
package reg

import (
	"mtac/commands"
	"mtac/logging"
	"runtime"
	"strconv"
	"strings"

	ps "github.com/bhendo/go-powershell"
	"github.com/bhendo/go-powershell/backend"
)

//implements 'commander'

// Struct with multiple parameters, that can be used with the Create function.
//config
type Registry struct {
	cmdBase commands.Cmd
	path    string
	key     string
}

// Create is the function, that initializes the struct and checks the syntax.
func (r Registry) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	if strings.EqualFold(runtime.GOOS, "windows") {
		struc := Registry{cmdBase: cmd}
		if temp, ok := m["path"].(string); !ok {
			logger.Error("'path' misspelled or not available.")
			return false, struc
		} else if temp == "" {
			logger.Error("'path' empty.")
			struc.path = temp
			return false, struc
		} else {
			struc.path = temp
		}
		if temp, ok := m["key"].(string); !ok {
			logger.Error("'key' misspelled or not available.")
			return false, struc
		} else if temp == "" {
			logger.Error("'Key' empty.")
			struc.key = temp
			return false, struc
		} else {
			struc.key = temp
		}
		return true, struc
	} else {
		logger.Errorf("The given Os is %s, but the command is only implemented for windows.", runtime.GOOS)
		return false, Registry{cmdBase: cmd}
	}
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (r Registry) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(r, m, logger)
}

// TestPath test if a path exists in the registry. It returns true, if it does. It is required to distinguish between a HKLM or HKCU environment.
func TestPath(path string, names map[string]string) bool {
	var logger = logging.CommandLogger(names)
	back := &backend.Local{}

	// start a local powershell process
	shell, err := ps.New(back)
	if err != nil {
		logger.Error(err)
	}
	defer shell.Exit()

	// Execute powershell command
	output, _, _ := shell.Execute("Test-Path -Path 'registry::" + path + "'")

	// Trim output
	output = strings.TrimSpace(output)

	// Evaluate the output
	isRegKey := false

	if len(output) > 0 {
		// Parse output to bool
		isRegKey, err = strconv.ParseBool(strings.TrimSpace(output))
		if err != nil {
			logger.Error(err, ": ", string(output))
		}
	}
	return isRegKey
}

// Execute exeutes a powershell command and finds out the HK environment with the TestPath function.
func (r Registry) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}

	// Path does not exists
	if pathExists := TestPath(r.path, names); !pathExists {
		message := "Path was not found: " + r.path
		a.Result = message
		logger.Warn(message)
		return a, true
	}

	// Run Powershell command, if the path exists
	back := &backend.Local{}
	shell, err := ps.New(back)
	if err != nil {
		logger.Error(err)
	}
	defer shell.Exit()

	// Execute GetItemProperty
	output, _, err := shell.Execute("Get-ItemProperty -path 'registry::" + r.path + "' | Select '" + r.key + "'")
	if err != nil {
		// Save error as output
		a.Result = err.Error()

		// Handle Permission denied
		if strings.Contains(err.Error(), "Requested registry access is not allowed.") {
			logger.Error("Requested registry access is not allowed. : " + r.path + "\\" + r.key)
			return a, false
		}

		logger.Error(err, ": ", string(output))
		return a, false
	}

	// Parse output
	output = output[strings.LastIndex(output, "-")+1:]
	output = strings.TrimSpace(output)

	if output == "" {
		// The key was not found
		message := "Key was not found: " + r.key
		a.Result = message
		logger.Warn(message)
	} else {
		a.Result = output
	}

	return a, true

}
