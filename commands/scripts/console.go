package scripts

import (
	"mtac/logging"

	"github.com/dop251/goja"
)

// type Console is a struct for the console.
type Console struct{}

/*
	NewConsole returns a new Console-Object upon being called.
*/
func NewConsole(vm *goja.Runtime) *Console {
	c := &Console{}
	return c
}

/*
	Log prints out Logging-Statement to Console.
*/
func (c Console) Log(msg string) {
	logger := logging.SimpleLogger()
	logger.Println(msg)
}
