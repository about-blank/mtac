package testmount

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// General tests
	{M: map[interface{}]interface{}{}, Res: true},

	// dev
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "", "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"dev": true, "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"dev": false, "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"dev": "true", "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: true},

	// regex
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": false, "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": "true", "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": "false", "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": "foo", "isSet": true, "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"dev": "foo", "isSet": true, "options": []interface{}{"foobar"}}, Res: true},

	//isSet
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": "true", "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": false, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": "true", "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": "", "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": "foo", "options": []interface{}{"foobar"}}, Res: false},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "options": []interface{}{"foobar"}}, Res: true},

	//options
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{"foobar"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true}, Res: false},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{"foobar", "abc"}}, Res: true},
	{M: map[interface{}]interface{}{"dev": "foo", "regex": true, "isSet": true, "options": []interface{}{true}}, Res: false},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, TestMount{})
}
