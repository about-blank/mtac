package oschecker

import (
	"mtac/logging"

	"github.com/zcalusic/sysinfo"
)

// GetVersion uses an import to check on which linux distribution the user runs.
func GetVersion() string {
	logger := logging.SimpleLogger()
	var si sysinfo.SysInfo

	si.GetSysInfo()

	distro := si.OS.Vendor
	if distro != "" {
		return distro
	} else {
		logger.Errorf("Can't get distro infos of %s.", distro)
		return ""
	}
}

// getOS is equivalent to getVersion on Unix-Systems.
func getOS() string {
	return GetVersion()
}
