// Package gpo contains the create, verify and execute functions for the command.
package gpo

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"mtac/commands"
	"mtac/logging"
	"mtac/output"
	"os"
	"runtime"
	"strings"
	"unicode/utf16"
	"unicode/utf8"

	ps "github.com/bhendo/go-powershell"
	"github.com/bhendo/go-powershell/backend"
)

// Struct with multiple parameters, that can be used with the Create function.
type GPO struct {
	cmdBase commands.Cmd
	name    string
}

// Required structs for the XML-Import
type XMLQuery struct {
	XMLName xml.Name `xml:"Rsop"`
	GPOs    []XMLGPO `xml:"UserResults>GPO"`
}

type XMLGPO struct {
	Name            string `xml:"Name"`
	IsEnabled       bool   `xml:"Enabled"`
	IsValid         bool   `xml:"IsValid"`
	IsFilterAllowed bool   `xml:"FilterAllowed"`
}

// Create is the function, that initializes the struct and checks the syntax.
func (gpo GPO) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := GPO{cmdBase: cmd}
	if temp, ok := m["name"].(string); !ok {
		logger.Error("'Name' misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Warn("'Name' empty.")
		struc.name = temp
	} else {
		struc.name = temp
		return true, struc
	}
	return false, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (gpo GPO) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(gpo, m, logger)
}

// Execute gets information about the group policy objects on windows.
func (gpo GPO) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    true,
		Result:    "",
		Filepath:  output.GPResultXMLPath,
	}

	// Make sure, that the runtime OS is Windows
	if strings.EqualFold(runtime.GOOS, "linux") {
		logger.Errorf("GPO can only be executed on Windows. Not on '%s'.", runtime.GOOS)
		return a, false
	}

	back := &backend.Local{}

	// Start a local powershell process
	shell, err := ps.New(back)
	if err == nil {

		// Check if gpresult already was executed
		if _, err := os.Stat(output.GPResultXMLPath); os.IsNotExist(err) {

			// Execute powershell command
			_, _, err := shell.Execute("gpresult /X " + output.GPResultXMLPath)

			// Exit shell & check Errors
			defer shell.Exit()
			if err != nil {
				logger.Error("Can't execute gpresult:", err)
				return a, false
			}
		}

		// Read XML-File
		dataUTF16, _ := ioutil.ReadFile(output.GPResultXMLPath)

		// Change encoding to UTF-8
		dataUTF8 := DecodeUTF16(dataUTF16)
		dataUTF8 = strings.Replace(dataUTF8, "<?xml version=\"1.0\" encoding=\"utf-16\"?>", "", 1)

		// Map XML to struct
		query := &XMLQuery{}
		_ = xml.Unmarshal([]byte(dataUTF8), &query)

		// Decide if the gpo is applied
		for _, tempGPO := range query.GPOs {
			if tempGPO.Name == gpo.name {
				if tempGPO.IsEnabled && tempGPO.IsFilterAllowed && tempGPO.IsValid {
					// GPO is applied
					a.Result = string("Group Policy: " + tempGPO.Name + " is applied!")
					return a, true
				}
			}
		}

		// Not applied
		a.Result = string("Group Policy: " + gpo.name + " is not applied!")
		return a, false
	} else {
		logger.Error("Can't spawn powershell process:", err)
		return a, false
	}
}

// Decodes a byte error from UTF-8 to UTF-16
func DecodeUTF16(b []byte) string {

	if len(b)%2 != 0 {
		return ""
	}

	u16s := make([]uint16, 1)

	ret := &bytes.Buffer{}

	b8buf := make([]byte, 4)

	lb := len(b)
	for i := 0; i < lb; i += 2 {
		u16s[0] = uint16(b[i]) + (uint16(b[i+1]) << 8)
		r := utf16.Decode(u16s)
		n := utf8.EncodeRune(b8buf, r[0])
		ret.Write(b8buf[:n])
	}

	return ret.String()
}
