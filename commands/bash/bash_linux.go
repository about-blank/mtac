package bash

import (
	"mtac/commands"
	"mtac/logging"
	"os/exec"
	"strings"
)

// Execute is the function to execute the command. It executes a string on a virtual bash and is able to compare it with the result.
func (b Bash) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	cmd := string(b.input)
	bash := exec.Command("bash", "-c", cmd)
	out, err := bash.CombinedOutput()
	out = []byte(strings.TrimSuffix(string(out), "\n"))
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    string(out),
		Filepath:  "",
	}
	if err != nil {
		logger.Errorf("Problem executing Bash. %s. Output: %s.", err, string(out))

		return a, false
	}
	// resolves null problem with empty string. The user gives a null input, so he does not want to give any expected result
	if b.expectedResult == "z1rPLHTH1UJLkYO" {
		return a, true
	}
	if string(out) == b.expectedResult {
		return a, true
	}
	logger.Warnf("Expected: '%s'. Got: '%s'.", b.expectedResult, string(out))
	return a, false
}
