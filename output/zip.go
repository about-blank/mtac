package output

import (
	"archive/zip"
	"bufio"
	"io/ioutil"
	"mtac/logging"
	"os"
	"strings"
)

// CreateZipArchive takes all files and folders form the temp directory and writes them into a Zip-Archive.
func CreateZipArchive() {
	logger := logging.SimpleLogger()

	// Get a Buffer to Write To
	bufferFile, err := os.Create(OutputZipPathAndName)
	if err != nil {
		logger.Println(err)
	}
	defer bufferFile.Close()

	// Create a new zip archive.
	writer := zip.NewWriter(bufferFile)

	// Add some files to the archive.
	addFiles(writer, TempDir, "")
	if err != nil {
		logger.Error(err)
	}

	// Make sure to check the error on Close.
	err = writer.Close()
	if err != nil {
		logger.Error(err)
	}

	Debugfile.Close()
	err = os.RemoveAll(TempDir)
	if err != nil {
		logger.Error(err)
	}

}

// addFiles adds files to the ZipArchive.
func addFiles(writer *zip.Writer, basePath string, baseInZip string) {
	logger := logging.SimpleLogger()

	// Open the Directory
	files, err := ioutil.ReadDir(basePath)
	if err != nil {
		logger.Error(err)
	}

	for _, file := range files {
		if !file.IsDir() {
			// Check Zipignore in TempDir
			if basePath == TempDir && containsZipIgnore(file.Name()) {
				continue
			}

			dat, err := ioutil.ReadFile(basePath + file.Name())
			if err != nil {
				logger.Error(err)
			}
			// Add some files to the archive.
			f, err := writer.Create(baseInZip + file.Name())
			if err != nil {
				logger.Error(err)
			}
			if f != nil {
				_, err = f.Write(dat)
				if err != nil {
					logger.Error(err)
				}

			}

		} else if file.IsDir() {

			// Recursive call for directories
			newBase := basePath + file.Name() + "/"
			addFiles(writer, newBase, baseInZip+file.Name()+"/")
		}

	}
}

// Check if the specified file is also mentioned in the zip ignore.
func containsZipIgnore(fileName string) bool {
	logger := logging.SimpleLogger()

	// Open zipignore
	file, err := os.Open(workingDirectory + "/output/zipignore")
	if err != nil {
		logger.Error(err)
	}
	defer file.Close()

	// Check if the file is mentioned
	containsZipIgnore := false
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == fileName {
			containsZipIgnore = true
		}
	}

	// Handle errors
	if err := scanner.Err(); err != nil {
		logger.Error(err)
	}

	return containsZipIgnore
}
