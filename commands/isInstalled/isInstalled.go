// Package isInstalled contains the create, verify and execute functions for the command.
package isInstalled

import (
	"mtac/commands"
	"mtac/logging"

	"github.com/sirupsen/logrus"
)

//implements 'commander'

// Struct with multiple parameters, that can be used with the Create function.
type IsInstalled struct {
	cmdBase     commands.Cmd
	packageName string
}

// Create is the function, that initializes the struct and checks the syntax.
func (i IsInstalled) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := IsInstalled{cmdBase: cmd}
	if temp, ok := m["packageName"].(string); !ok {
		logger.Error("'PackageName' misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Error("'packageName' is empty.")
		struc.packageName = temp
		return false, struc
	} else {
		struc.packageName = temp
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (i IsInstalled) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(i, m, logger)
}

// existsCheck checks, that the given string is really empty.
func existsCheck(s string, err error) string {

	if s == "" {
		return ""
	}

	return s
}

// emptyA returns an empty artifact, if the package is not installed.
func emptyA(names map[string]string, pkgName string, logger *logrus.Entry) (commands.Artifact, bool) {
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    pkgName + " is not installed.",
		Filepath:  "",
	}
	logger.Warnf("'%s' is not installed.", pkgName)
	return a, false
}
