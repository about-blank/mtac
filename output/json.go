package output

import (
	"encoding/json"
	"io/ioutil"
	"mtac/logging"
)

// Structs for the JSON logging
type Task struct {
	Command      string
	ArtifactPath string
	ResultPath   string
	Success      bool
}

type Audit struct {
	Name  string
	Tasks []Task
}

type Module struct {
	Name   string
	Audits []Audit
}

type Log struct {
	OperatingSystem string
	Customer        string
	Modules         []Module
}

var Data Log = Log{}

const fileName string = "output.json"

// CreateJSONLog logs json to file.
func CreateJSONLog() {
	logger := logging.SimpleLogger()

	// Convert raw data to JSON
	jsonData, _ := json.MarshalIndent(Data, "", " ")

	// Write JSON to file
	err := ioutil.WriteFile(TempDir+fileName, jsonData, 0664)
	if err != nil {
		logger.Errorf("Can't create %s. %v", TempDir+fileName, err)
	}
}

// logContainsModule iterates through the Data.Modules and returns true, if a module with this name already exists.
func logContainsModule(name string) bool {
	for i := range Data.Modules {
		if Data.Modules[i].Name == name {
			return true
		}
	}
	return false
}

// moduleContainsAudit iterates through module.Audits and returns true, if an audit with this name already exists.
func moduleContainsAudit(name string, module Module) bool {
	for i := range module.Audits {
		if module.Audits[i].Name == name {
			return true
		}
	}
	return false
}

// AddTaskToLog writes the given Task to the log data.
func AddTaskToLog(task Task, auditName string, moduleName string) {

	// Make sure the log Contains the current module
	if !logContainsModule(moduleName) {
		Data.Modules = append(Data.Modules, Module{Name: moduleName})
	}

	// Find the module
	for i := range Data.Modules {
		if Data.Modules[i].Name == moduleName {

			// Make sure the module Contains the current audit
			if !moduleContainsAudit(auditName, Data.Modules[i]) {
				Data.Modules[i].Audits = append(Data.Modules[i].Audits, Audit{Name: auditName})
			}

			// Find the audit
			for j := range Data.Modules[i].Audits {
				if Data.Modules[i].Audits[j].Name == auditName {
					// Add Task
					Data.Modules[i].Audits[j].Tasks = append(Data.Modules[i].Audits[j].Tasks, task)
				}
			}
		}

	}
}
