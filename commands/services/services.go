// Package services contains the create, verify and execute functions for the command.
package services

import (
	"mtac/commands"
	"mtac/logging"
	"strconv"
)

//implements 'commander'

// Struct with multiple parameters, that can be used with the Create function.
type Services struct {
	cmdBase commands.Cmd
	enabled bool
	running bool
	service string
}

// Create is the function, that initializes the struct and checks the syntax.
func (s Services) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := Services{cmdBase: cmd}
	if temp := m["enabled"]; temp != nil {
		if res, ok := temp.(bool); !ok {
			cs, err := strconv.ParseBool(temp.(string))
			if err != nil {
				logger.Errorf("'%s' is no valid boolean.\n", temp)
				return false, struc
			}
			struc.enabled = cs
		} else {
			struc.enabled = res
		}
	}
	if temp := m["running"]; temp != nil {
		if res, ok := temp.(bool); !ok {
			cs, err := strconv.ParseBool(temp.(string))
			if err != nil {
				logger.Errorf("'%s' is no valid boolean.\n", temp)
				return false, struc
			}
			struc.running = cs
		} else {
			struc.running = res
		}
	}
	if temp, ok := m["service"].(string); !ok {
		logger.Error("'service' misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Error("'service' empty.")
		struc.service = temp
		return false, struc
	} else {
		struc.service = temp
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (s Services) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(s, m, logger)
}
