package parser

import (
	"io/ioutil"
	"mtac/commands"
	"mtac/commands/oschecker"
	"mtac/output"
	"syscall"
	"testing"

	"github.com/spf13/viper"
)

func TestSetConf(t *testing.T) {

	// Test normal File
	f, _ := ioutil.TempFile("", "testmainconf")
	ioutil.WriteFile(f.Name(), []byte(`OS: Ubuntu`), 0644)
	SetConf(f.Name())

	// empty string
	standardConfig = "./../conf/mtac-conf/mtacAll.yaml"
	SetConf("")

	// broken file
	g, _ := ioutil.TempFile("", "testmainconf")
	ioutil.WriteFile(g.Name(), []byte(`OS:`), 0644)
	SetConf(g.Name())
}

type parseStruc struct {
	names    map[string]string
	commands string
	cmd      commands.Cmd
	m        map[interface{}]interface{}
	res      bool
}

var namesTest = map[string]string{
	"TaskName":  "foo",
	"ModulName": "bar",
	"audit":     "foo",
	"task":      "bar",
}
var cmd = commands.Cmd{}

var parseStrucList = []parseStruc{

	// Working tests
	{namesTest, "bash", cmd, map[interface{}]interface{}{"input": "uname -r"}, true},
	{namesTest, "filePermission", cmd, map[interface{}]interface{}{"fileName": "/etc/passwd"}, true},
	{namesTest, "find", cmd, map[interface{}]interface{}{"dir": "./"}, true},

	//names
	{make(map[string]string), "bash", cmd, map[interface{}]interface{}{"input": "uname -r"}, true},

	// commands
	{namesTest, "bAsh", cmd, map[interface{}]interface{}{"input": "uname -r"}, true},
	{namesTest, "Bash", cmd, map[interface{}]interface{}{"input": "uname -r"}, true},
	{namesTest, "bsh", cmd, map[interface{}]interface{}{"input": "uname -r"}, false},
	{namesTest, "", cmd, map[interface{}]interface{}{"input": "uname -r"}, false},

	// map
	{namesTest, "bash", cmd, map[interface{}]interface{}{"": ""}, false},
	{namesTest, "bash", cmd, map[interface{}]interface{}{}, false},
}

func TestExecFuncs(t *testing.T) {
	for i, k := range parseStrucList {
		_, res := execFuncs(k.names, k.commands, k.cmd, k.m)
		if res != k.res {
			t.Errorf("ERROR: The test nr. %v did not work. Content: %s\n", i, k.m)
		}
	}
}

var verifyMap = []parseStruc{
	// INFO: Few tests, because most inputs are getting tested in other test files
	{nil, "bash", cmd, map[interface{}]interface{}{"input": "foo"}, true},
	{nil, "bah", cmd, map[interface{}]interface{}{"input": "foo"}, false},
	{nil, "", cmd, map[interface{}]interface{}{"input": "foo"}, false},
	{nil, "foo", cmd, map[interface{}]interface{}{"input": "foo"}, false},
	{nil, "input", cmd, map[interface{}]interface{}{"input": "foo"}, false},
}

func TestVerifyFuncs(t *testing.T) {
	output.InitLog()
	for i, k := range verifyMap {
		res := verifyFuncs(k.commands, k.m, make(map[string]string))
		if res != k.res {
			t.Errorf("ERROR: The test nr. %v did not work. Content: %s\n", i, k.m)
		}
	}
}

func TestRecLoop(t *testing.T) {
	f, err := ioutil.TempFile("", "testmainconf")
	output.Data.OperatingSystem = oschecker.GetVersion()
	if err != nil {
		panic(err)
	}
	defer syscall.Unlink(f.Name())
	ioutil.WriteFile(f.Name(), []byte(
		`OS: Ubuntu
Customer: Pfennig
Modules:
  - name: Testblock
    tasks:
      - audit: Einzelner Befehl
        commands:
          - cmd: testMod
            properties:
              module: cramfs
`), 0644)
	viper.AddConfigPath(".")
	viper.SetConfigType("yaml")
	viper.SetConfigFile(f.Name())

	if err := viper.ReadInConfig(); err != nil {
		t.Fail()
	}

	Parser()
}
