package gpo

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// General tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"name": "foo"}, Res: true},

	// name tests
	{M: map[interface{}]interface{}{"name": ""}, Res: false},
	{M: map[interface{}]interface{}{"name": true}, Res: false},
	{M: map[interface{}]interface{}{"name": "true"}, Res: true},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, GPO{})
}
