// Package testMount contains the create, verify and execute functions for the command.
package testmount

import (
	"mtac/commands"
	"mtac/commands/grep"
	"mtac/logging"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
)

//implements 'commander'

// Struct with multiple parameters, that can be used with the Create function.
type TestMount struct {
	cmdBase commands.Cmd
	dev     string
	regex   bool
	isSet   bool
	options []string
}

// Create is the function, that initializes the struct and checks the syntax.
func (t TestMount) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := TestMount{cmdBase: cmd}
	if temp := m["regex"]; temp != nil {
		if res, ok := temp.(bool); !ok {
			r, err := strconv.ParseBool(temp.(string))
			if err != nil {
				logger.Errorf("'%s' is no valid boolean.\n", temp)
				return false, struc
			}
			struc.regex = r
		} else {
			struc.regex = res
		}
	}
	if temp := m["isSet"]; temp != nil {
		if res, ok := temp.(bool); !ok {
			r, err := strconv.ParseBool(temp.(string))
			if err != nil {
				logger.Errorf("'%s' is no valid boolean.\n", temp)
				return false, struc
			}
			struc.isSet = r
		} else {
			struc.isSet = res
		}
	}
	if m["dev"] != nil {
		if temp, ok := m["dev"].(string); !ok {
			logger.Error("'dev' misspelled or not available.")
			return false, struc
		} else {
			struc.dev = temp
		}
	}
	if m["dev"] == nil {
		logger.Info("'dev' empty.")
	}
	if m["options"] != nil {
		rangeOptions := m["options"].([]interface{})
		struc.options = make([]string, len(rangeOptions))
		for k := range rangeOptions {
			if rangeOptions[k] == "" {
				logger.Error("Empty option in TestMount.")
			}
			temp, ok := rangeOptions[k].(string)
			if !ok {
				logger.Error("Options in TestMount has syntax problems.")
				return false, struc
			}
			struc.options[k] = temp
		}
	} else if m["options"] == nil || m["options"] == "" {
		logger.Info("No options for Testmount were given.")
	}
	if struc.dev != "" && struc.options == nil {
		logger.Error("There is a device given, but no options.")
		return false, struc
	}
	if struc.dev == "" && len(struc.options) != 0 {
		logger.Error("There are options given, but no device.")
		return false, struc
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (t TestMount) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(t, m, logger)
}

/* Execute - Tests if devices specified either in 'regex' or 'mounted' are mounted. Done by executing 'mount' and validating its output by searching for occurrences of these devices.
 * If mount-options such as 'nosuid' or 'noexec' are present output will be search for occurrences these. Returns true if given values are present in 'mount'-output.
 */
func (tM TestMount) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	if strings.EqualFold(runtime.GOOS, "windows") {
		logger.Error("Testmount is not implemented for Windows")
		return commands.Artifact{}, false
	}
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}
	mountOutput, err := exec.Command("mount").Output()
	if mountOutput != nil && err == nil {
		if tM.dev == "" && tM.options == nil {
			a.Result = string(mountOutput)
			return a, true
		}
		tmp := grep.GrepTextInString(string(mountOutput), tM.regex, tM.dev, names)
		a.Result = tmp
		if tM.options == nil {
			return a, true
		} else {
			test := true
			for _, element := range tM.options {
				if !tM.isSet {
					// tests for each mount-option if present
					if grep.GrepTextInString(tmp, false, element, names) != "" {
						logger.Warnf("%s is not present.", element)

						// if not 'test'-default-values is set to false
						test = false
					}
				} else {

					// tests for each mount-option if present
					if grep.GrepTextInString(tmp, false, element, names) == "" {
						logger.Warnf("%s is not present.", element)

						// if not 'test'-default-values is set to false
						test = false
					}
				}

			}
			return a, test
		}
	} else {
		logger.Error("Problem in Module: " + a.ModulName + ", Task: " +
			a.TaskName + ". Either an error occurred or TestMount-Properties are incorrect.")
	}
	return a, false
}
