// Package scripts is the package, that holds all stuff for scripting.
package scripts

import (
	"mtac/commands"
	"mtac/logging"
	"strings"

	"github.com/dop251/goja"
)

// Struct with multiple parameters, that can be used with the Create function.
type Script struct {
	script         string
	function       string
	expectedResult string
}

var Scripts string
var ScriptableResults []interface{}

// Create is the function, that initializes the struct and checks the syntax.
func (s Script) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := Script{}
	if temp, ok := m["script"].(string); !ok {
		logger.Info("There was an Error creating the Script! Script misspelled or not available.")
	} else if temp == "" {
		logger.Info("Script got an empty script.")
		struc.script = temp
	} else {
		struc.script = temp
	}
	if temp, ok := m["function"].(string); !ok {
		logger.Error("'Function' misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Error("'Function' empty.")
		struc.function = temp
		return false, struc
	} else {
		struc.function = temp
	}
	if temp, ok := m["expectedResult"].(string); !ok {
		logger.Info("There was an Error creating the Script! ExpectedResult misspelled or not available.")
		struc.expectedResult = "z1rPLHTH1UJLkYO"
	} else if temp == "" {
		logger.Error("'ExpectedResult' empty.")
		struc.expectedResult = temp
		return false, struc
	} else {
		struc.expectedResult = temp
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (s Script) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(s, m, logger)
}

/*
	Execute executes a script-audit as specified in config.
	Is able to access all mtac native modules and previous results within the same task.
*/
func (s Script) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	a := commands.Artifact{
		ModulName: names["ModuleName"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}

	// Appends scripts as defined in config-script-module to the script-string
	s.script = Scripts + "\n" + s.script

	// initializing new goja runtime
	runtime := goja.New()

	// converts go-function-syntax to ecma-script-function-syntax and vice versa
	runtime.SetFieldNameMapper(
		goja.UncapFieldNameMapper(),
	)

	// initalizing objects associated with functions
	c := NewConsole(runtime)
	f := NewFile(runtime)
	m := NewM(runtime)

	// links each object with a string representation inside the goja-runtime
	runtime.Set("console", c)
	runtime.Set("file", f)
	runtime.Set("mtac", m)
	prevResults := runtime.NewArray(ScriptableResults...)

	// transfers the results from previous audits into the scope of a script-audit
	runtime.Set("results", prevResults)

	// commits the ecma-script-code as defined in config to the runtime
	_, err1 := runtime.RunString(s.script)
	setNamesMap(names)

	// calls the function specified in a scripting-audit and captures its return value
	value, err2 := runtime.RunString(s.function)

	if err1 == nil && err2 == nil {
		// saves function-return-value, if no errors occurred
		a.Result = value.String()
	}

	if err1 != nil {
		logger.Error("Initializing Scripts failed: ", err1)
	}

	if err2 != nil {
		logger.Error(err2)
	}

	// Trims leading and trailing whitespaces
	s.expectedResult = strings.TrimSpace(s.expectedResult)
	if s.expectedResult == "z1rPLHTH1UJLkYO" {
		return a, true
	} else {
		// compares the given result with the expected one. Matching = true and vice versa
		if a.Result == s.expectedResult {
			return a, true
		} else {
			return a, false
		}
	}
}
