package services

import (
	"mtac/commands/oschecker"
	"mtac/commands/testfuncs"
	"strings"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// general tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"enabled": true, "running": true, "service": ""}, Res: false},

	// service tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"service": ""}, Res: false},
	{M: map[interface{}]interface{}{"service": true}, Res: false},
	{M: map[interface{}]interface{}{"service": "foo"}, Res: true},

	// enabled tests
	{M: map[interface{}]interface{}{"service": "foo"}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "enabled": true}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "enabled": "true"}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "enabled": false}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "enabled": "false"}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "enabled": ""}, Res: false},
	{M: map[interface{}]interface{}{"service": "foo", "enabled": "bar"}, Res: false},

	// running tests
	{M: map[interface{}]interface{}{"service": "foo"}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "running": true}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "running": "true"}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "running": false}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "running": "false"}, Res: true},
	{M: map[interface{}]interface{}{"service": "foo", "running": ""}, Res: false},
	{M: map[interface{}]interface{}{"service": "foo", "running": "bar"}, Res: false},
}

func CreateTest(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, Services{})
}

func TestExec_services_windows(t *testing.T) {
	if strings.EqualFold(oschecker.GetOS(), "Windows") {
		testBool := false
		names := map[string]string{
			"ModulName": "Modulname",
			"TaskName":  "Taskname",
		}

		testService1 := Services{
			service: "Wcmsvc",
			running: true,
		}

		_, testBool = testService1.Execute(names)

		if testBool == false {
			t.Error("Execute func in services_windows is faulty")
		}
	}
}
