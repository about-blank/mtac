package filePermission

import (
	"mtac/commands"
	"mtac/logging"
	"os"
)

// Execute is the function to execute the command. It checks, who as access to a file.
func (f FilePermission) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	logger.Debug("Execute FilePermission.")
	info, err := os.Lstat(f.fileName)
	var result string

	if err == nil {
		fileMode := info.Mode()
		permissions := fileMode.String()

		// we are not in linux, this won't work anyway in windows,
		result = ("Permissions:" + permissions + ", Owner: unknown due to OS being Windows, Group: unknown due to OS being Windows")

		a := commands.Artifact{
			ModulName: names["name"],
			TaskName:  names["audit"],
			IsFile:    false,
			Result:    result,
			Filepath:  "",
		}

		return a, true
	} else {
		result = "Something went wrong:\n" + err.Error()
		a := commands.Artifact{
			ModulName: names["name"],
			TaskName:  names["audit"],
			IsFile:    false,
			Result:    result,
			Filepath:  "",
		}
		logger.Error(info, err.Error())
		return a, false
	}

}
