// Package logging contains the dedicated logger for commands and general logging.
package logging

import (
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
)

var LoggerInit *logrus.Logger

/*
	CommandLogger returns a custom logger for a command / a script.
*/
func CommandLogger(names map[string]string) *logrus.Entry {
	if LoggerInit == nil {
		LoggerInit = logrus.New()
	}
	var funcname string
	var filename string
	module, ok := names["name"]
	if !ok {
		module = ""
	}
	task, ok := names["audit"]
	if !ok {
		task = ""
	}
	bar, file, _, ok := runtime.Caller(1)
	foo := runtime.FuncForPC(bar).Name()
	if ok {
		s := strings.Split(foo, ".")
		funcname = s[len(s)-1]
		_, filename = path.Split(file)
		filename = strings.TrimSuffix(filename, filepath.Ext(filename))

	} else {
		funcname = ""
		filename = ""
	}
	var contextLogger *logrus.Entry
	contextLogger = LoggerInit.WithFields(logrus.Fields{
		"Module":   module,
		"Task":     task,
		"FileName": filename,
		"Function": funcname,
	})

	if module == "" && task == "" {
		contextLogger = LoggerInit.WithFields(logrus.Fields{
			"origin": "Scripting",
		})
	}
	return contextLogger
}

/*
	SimpleLogger returns a logger for simple logging with only the name of the command and no additional input.
*/
func SimpleLogger() *logrus.Entry {
	if LoggerInit == nil {
		LoggerInit = logrus.New()
	}
	var funcname string
	var filename string
	bar, file, _, ok := runtime.Caller(1)
	name := runtime.FuncForPC(bar).Name()
	var simpleLogger *logrus.Entry
	if ok {
		s := strings.Split(name, ".")
		funcname = s[len(s)-1]
		_, filename = path.Split(file)
		filename = strings.TrimSuffix(filename, filepath.Ext(filename))
		simpleLogger = LoggerInit.WithFields(logrus.Fields{
			"FileName": filename,
			"Function": funcname,
		})
	}
	return simpleLogger
}

/*
	SimpleLogger returns a logger for simple logging with only the name of the command and no additional input.
*/
func MinimalLogger() *logrus.Entry {
	if LoggerInit == nil {
		LoggerInit = logrus.New()
	}
	var filename string
	_, file, _, ok := runtime.Caller(1)
	var minimalLogger *logrus.Entry
	if ok {
		_, filename = path.Split(file)
		filename = strings.TrimSuffix(filename, filepath.Ext(filename))
		minimalLogger = LoggerInit.WithFields(logrus.Fields{
			"FileName": filename,
		})
	}
	return minimalLogger
}

func SetLogLevel(loglevel string) bool {
	var levelmap = map[string]logrus.Level{
		"Trace":   logrus.TraceLevel,
		"Debug":   logrus.DebugLevel,
		"Info":    logrus.InfoLevel,
		"Warning": logrus.WarnLevel,
		"Error":   logrus.ErrorLevel,
		"Fatal":   logrus.FatalLevel,
		"Panic":   logrus.PanicLevel,
	}

	for k, v := range levelmap {
		if strings.EqualFold(k, loglevel) {
			LoggerInit.SetLevel(v)
			return true
		}
	}
	LoggerInit.SetLevel(logrus.InfoLevel)
	return false
}

func CMDLineLogger(names map[string]string) *logrus.Entry {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		TimestampFormat: "15:04:05",
		FullTimestamp:   true,
	})
	var CMDLineLogger *logrus.Entry
	module, ok := names["name"]
	if !ok {
		module = ""
	}
	task, ok := names["audit"]
	if !ok {
		task = ""
	}
	log.SetOutput(os.Stdout)
	if ok {
		CMDLineLogger = log.WithFields(logrus.Fields{
			"Module": module,
			"Task":   task,
		})
	}

	return CMDLineLogger
}

func PanicLogger(err error) {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		TimestampFormat: "15:04:05",
		FullTimestamp:   true,
	})

	temp, _ := os.Getwd()

	err2 := os.RemoveAll(temp + "/temp/")
	if err2 != nil {
		log.Error(err2)
	}

	log.Fatal(err)

}
