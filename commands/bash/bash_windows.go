package bash

import (
	"mtac/commands"
	"mtac/logging"

	ps "github.com/bhendo/go-powershell"
	"github.com/bhendo/go-powershell/backend"
)

// Execute is the function to execute the command. It executes a string on a virtual bash and is able to compare it with the result.
func (b Bash) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}
	back := &backend.Local{}

	// Start a local powershell process
	shell, err := ps.New(back)
	if err == nil {

		// Execute powershell command
		out, _, err := shell.Execute(b.input)

		// Save the output
		a.Result += out

		defer shell.Exit()

		// Error logging
		if err != nil {
			logger.Errorf("Problem executing Bash. %s. Output: %s.", err, string(out))

			return a, false
		}
		// resolves null problem with empty string. The user gives a null input, so he does not want to give any expected result
		if b.expectedResult == "z1rPLHTH1UJLkYO" {
			return a, true
		}
		if string(out) == b.expectedResult {
			return a, true
		}
		logger.Warnf("Expected: '%s'. Got: '%s'.", b.expectedResult, string(out))
		return a, false

	} else {

		logger.Errorf("Can't spawn powershell process: %v.", err)
		return a, false
	}
}
