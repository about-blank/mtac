package find

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"dir": ""}, Res: false},
	{M: map[interface{}]interface{}{"dir": "/etc/fstab"}, Res: true},
	{M: map[interface{}]interface{}{"dir": true}, Res: false},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, Find{})
}
