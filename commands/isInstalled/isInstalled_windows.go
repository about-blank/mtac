package isInstalled

import (
	"mtac/commands"
	"mtac/logging"

	ps "github.com/bhendo/go-powershell"
	"github.com/bhendo/go-powershell/backend"
)

// Execute checks, wether the package given by the map is installed on the operating system.
func (i IsInstalled) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	back := &backend.Local{}

	// start a local powershell process
	shell, err := ps.New(back)
	if err != nil {
		panic(err)
	}
	defer shell.Exit()

	resultOutput, _, err := shell.Execute("PowerShell -ExecutionPolicy Bypass -File ./commands/isInstalled/getInstalledSoftwareByName.ps1 -Name '" + i.packageName + "'")

	// Checks if resultOutput is empty or not
	if existsCheck(resultOutput, err) != "" {
		return mainCheck(i.packageName, resultOutput, names, err)
	} else {
		logger.Warn("Empty shell result.")
		return emptyA(names, i.packageName, logger)
	}
}

// mainCheck collects all the infos to create the Artifact, that is given to the output.
func mainCheck(pkgName string, result string, names map[string]string, err error) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	if err != nil {
		logger.Error(err)
	}

	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    true,
		Result:    "Result for " + pkgName + ": " + result,
		Filepath:  "",
	}

	return a, true
}
