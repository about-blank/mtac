package scripts

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// General tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"script": "foo", "function": "bar", "expectedResult": "foobar"}, Res: true},

	// Script tests
	{M: map[interface{}]interface{}{"script": "", "function": "bar", "expectedResult": "foobar"}, Res: true},
	{M: map[interface{}]interface{}{"script": true, "function": "bar", "expectedResult": "foobar"}, Res: true},
	{M: map[interface{}]interface{}{"script": "true", "function": "bar", "expectedResult": "foobar"}, Res: true},
	{M: map[interface{}]interface{}{"function": "bar", "expectedResult": "foobar"}, Res: true},

	// function tests
	{M: map[interface{}]interface{}{"script": "foo", "function": "", "expectedResult": "foobar"}, Res: false},
	{M: map[interface{}]interface{}{"script": "foo", "expectedResult": "foobar"}, Res: false},
	{M: map[interface{}]interface{}{"script": "foo", "function": "true", "expectedResult": "foobar"}, Res: true},
	{M: map[interface{}]interface{}{"script": "foo", "function": true, "expectedResult": "foobar"}, Res: false},

	// expResult tests
	{M: map[interface{}]interface{}{"script": "foo", "function": "bar", "expectedResult": ""}, Res: false},
	{M: map[interface{}]interface{}{"script": "foo", "function": "bar"}, Res: true},
	{M: map[interface{}]interface{}{"script": "foo", "function": "bar", "expectedResult": "true"}, Res: true},
	{M: map[interface{}]interface{}{"script": "foo", "function": "bar", "expectedResult": false}, Res: true},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, Script{})
}
