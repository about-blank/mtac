// Package output contains the functions for formatting and writing the results of mtac.
package output

import (
	"fmt"
	"io/ioutil"
	"mtac/commands"
	"mtac/logging"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
)

var (
	workingDirectory, _  = os.Getwd()
	TempDir              = os.TempDir() + "/mtac/"
	OutputZipPathAndName = workingDirectory
	GPResultXMLPath      = TempDir + "gpresult.xml"
	commandCounter       = 0
	commandsFailed       = 0
	commandsSuccessfull  = 0

	FailedAuditName = make(map[int]string)
	FailedAuditPath = make(map[int]string)
	ZipName         string

	start     time.Time
	Debugfile *os.File
)

// LogArtifact gets an artifact and writes the infos to a file.
func LogArtifact(a commands.Artifact, success, fromScript bool, command string) bool {
	logger := logging.SimpleLogger()

	// Disables logging of initial script call
	if a.ModulName == "" && a.TaskName == "" {
		return true
	}

	// Sanitize module & audit for folder creation
	blacklist := [...]string{"/", "\\"}
	replaceSymbol := "-"
	for _, element := range blacklist {
		a.ModulName = strings.ReplaceAll(a.ModulName, element, replaceSymbol)
		a.TaskName = strings.ReplaceAll(a.TaskName, element, replaceSymbol)
	}

	// Build the required directory structure
	taskDir := TempDir + "/" + a.ModulName + "/" + a.TaskName
	artifactDir := taskDir + "/Artifacts/"
	outputDir := taskDir + "/Outputs/"

	// Counter to organize the output
	if !fromScript {
		commandCounter++
		if success {
			commandsSuccessfull++
		}
	}

	// Create folders and check for errors
	err := os.MkdirAll(artifactDir, 0777)
	if err != nil {
		logger.Errorf("Error creating %s. %v", artifactDir, err)
	}

	err = os.MkdirAll(outputDir, 0777)
	if err != nil {
		logger.Errorf("Error creating %s. %v", outputDir, err)
	}

	// Generate file name
	uuid, _ := uuid.NewV4()
	fileName := uuid.String() + ".txt"

	if !fromScript {
		if !success {
			name := a.ModulName + "/" + a.TaskName

			FailedAuditName[commandsFailed] = name
			FailedAuditPath[commandsFailed] = path.Clean(strings.ReplaceAll(outputDir, TempDir, ZipName) + fileName)
			commandsFailed++
		}
	}
	// Log failed command

	// Declare function to write files
	writeContentToFile := func(content []byte, filePath string) {
		err := ioutil.WriteFile(filePath, content, 0664)
		if err != nil {
			logger.Errorf("Error creating %s. %v", filePath, err)
		}
	}

	// Save the output
	writeContentToFile([]byte(a.Result), outputDir+fileName)

	// Save the artifact file, if there is one:
	if len(a.Filepath) > 0 {

		// Check if the artifact exists
		if _, err := os.Stat(a.Filepath); !os.IsNotExist(err) {

			// Copy the referenced file into the artifact folder
			referencedFileContent, err := ioutil.ReadFile(a.Filepath)
			if err != nil {
				logger.Errorf("Error reading %s. %v", a.Filepath, err)
				return false
			}

			writeContentToFile(referencedFileContent, artifactDir+fileName)
		} else {
			logger.Warn("File does not exists: " + a.Filepath)
		}
	}

	// Prepare entry for JSON-Log
	task := Task{
		Command:    command,
		ResultPath: path.Clean(strings.ReplaceAll(outputDir, TempDir, ZipName) + fileName),
		Success:    success,
	}

	// Add the path to the artifact, if an artifact exists
	if len(a.Filepath) > 0 {
		task.ArtifactPath = artifactDir + fileName
	}

	// Create log data
	AddTaskToLog(task, a.TaskName, a.ModulName)

	return true
}

// SetOutputName sets the output name of the Zip-Archive wit the following pattern: mtac_<Customer>_<YYYY-MM-DD> T<HH-MM>
func SetOutputName(customer string) {

	// Get the current time and trim it into the required pattern
	var currentTime string = time.Now().String()
	var date string = currentTime[0:11]
	var time string = currentTime[11:16]
	currentTime = date + "T" + time
	currentTime = strings.Replace(currentTime, ":", "-", 2)

	// Build and set zip name
	ZipName = "mtac_" + customer + "_" + currentTime
	// Replace possible double underscore, if the customer is empty
	ZipName = strings.Replace(ZipName, "__", "_", -1)

	OutputZipPathAndName = OutputZipPathAndName + "/" + ZipName + ".zip"
}

// FinalizeDebug closes the open files and cleans after itself.
func FinalizeDebug(dryrun bool) {
	logger := logging.SimpleLogger()

	end := time.Now()
	duration := end.Sub(start)

	debug, err := os.OpenFile(TempDir+"debug.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		logger.Println(err)
	}
	debug_temp, err := ioutil.ReadFile(TempDir + "debug_temp.log")
	if err != nil {
		logger.Println(err)
	}

	var failedAudits string

	i := 0
	length := 0

	for i < len(FailedAuditName) {
		if length < len(FailedAuditName[i]) {
			length = len(FailedAuditName[i])
		}
		i++
	}

	length += 6

	i = 0
	if length%8 != 0 {
		length += (8 - (length % 8))
	}

	for i < len(FailedAuditName) {
		loopLength := len(FailedAuditName[i]) + 6
		if loopLength%8 != 0 {
			loopLength -= (loopLength % 8)
		}

		j := 0
		for j < (length - loopLength) {
			FailedAuditName[i] = FailedAuditName[i] + "\t"
			j += 8
		}
		failedAudits += "Name: " + FailedAuditName[i] + "\tPath: " + FailedAuditPath[i] + "\n"
		i++
	}
	var content string
	if !dryrun {
		content = "MTAC executed. Execution time:" + duration.String() + ". \n" +
			"" + strconv.Itoa(commandCounter) + " commands executed. \n" +
			"" + strconv.Itoa(commandsSuccessfull) + " commands were successful \n" +
			"" + strconv.Itoa(commandsFailed) + " commands failed \n" +
			"--------------------------------------------------------------- \n" +
			"Failed Audits: \n" +
			failedAudits +
			"--------------------------------------------------------------- \n" +
			"Logs: \n" +
			string(debug_temp)
	} else {
		content = string(debug_temp)
	}

	debug.Write([]byte(content))

	err = os.Remove(TempDir + "debug_temp.log")
	if err != nil {
		logger.Error(err)
	}

	fmt.Println(content)

	debug.Close()

}

/* InitLog  sets all the parameters for logging
 */
func InitLog() {

	// Create temp folder
	start = time.Now()
	err := os.MkdirAll(TempDir, 0777)
	log := logrus.New()
	if err != nil {
		log.Error(err)
	}

	log.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		TimestampFormat: "15:04:05",
		FullTimestamp:   true,
	})

	// Create & Truncate Debug.log
	Debugfile, err = os.OpenFile(TempDir+"debug_temp.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Error(err)
	}

	if err := os.Truncate(TempDir+"debug_temp.log", 0); err != nil {
		log.Error(err)
	}

	log.SetOutput(Debugfile)
	logging.LoggerInit = log
}
