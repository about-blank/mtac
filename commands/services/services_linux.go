package services

import (
	"mtac/commands"
	"mtac/logging"
	"os/exec"
	"regexp"

	"github.com/sirupsen/logrus"
)

// Execute receives a systemd-struct and checks its boolean statements by testing if the given service is enabled  and/or running. If both conditions are contextual true, it returns 'true'.
func (s Services) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	artEnabled, enabled := testEnabled(s.service, logger, s)
	artRunning, running := testRunning(s.service, logger, s)

	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    artEnabled + artRunning,
		Filepath:  "",
	}
	return a, (enabled == s.enabled && running == s.running)
}

// testsEnabled checks, if the service are enabled.
func testEnabled(service string, logger *logrus.Entry, s Services) (string, bool) {
	outputEnabled, err := exec.Command("systemctl", "is-enabled", service).Output()
	if err == nil {
		if string(outputEnabled) == "enabled\n" {
			return string(outputEnabled), true
		}
	} else {
		logger.Debug(err)
		if s.enabled {
			logger.Warnf("Service '%s' is not enabled.", service)
		}
	}

	return string(outputEnabled), false
}

// testRunning checks, if the service is running.
func testRunning(service string, logger *logrus.Entry, s Services) (string, bool) {
	outputRunning, err := exec.Command("systemctl", "status", service).Output()
	if err == nil {
		if matched, err := regexp.MatchString("Active: active", string(outputRunning)); err == nil && matched {
			reg := regexp.MustCompile("Main PID: ([0-9]+)")
			data := reg.FindStringSubmatch(string(outputRunning))
			if len(data) > 1 {
				return string(outputRunning), true
			}
		}
	} else {
		if s.running {
			logger.Warnf("Service '%s' is not running.", service)
		}
	}
	return string(outputRunning), false
}
