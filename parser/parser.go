// Package parser reads the config and executes the commands.
package parser

import (
	"mtac/commands"
	"mtac/commands/bash"
	"mtac/commands/filePermission"
	"mtac/commands/find"
	"mtac/commands/gpo"
	"mtac/commands/grep"
	"mtac/commands/isInstalled"
	"mtac/commands/oschecker"
	"mtac/commands/reg"
	"mtac/commands/scripts"
	"mtac/commands/services"
	"mtac/commands/testMod"
	"mtac/commands/testmount"
	"mtac/logging"
	"mtac/output"
	"sort"

	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	/* Changeable vars. Please change vars only here
	 */

	// Relative default path for the config
	standardConfig string = "conf/Ubuntu/MTACUbuntu.yaml"

	// Map with all the implemented commands. Need to be of type Commander
	commandMap = map[string]commands.Commander{
		"bash":           bash.Bash{},
		"filepermission": filePermission.FilePermission{},
		"find":           find.Find{},
		"gpo":            gpo.GPO{},
		"grep":           grep.Grep{},
		"registry":       reg.Registry{},
		"isinstalled":    isInstalled.IsInstalled{},
		"service":        services.Services{},
		"testmod":        testMod.TestMod{},
		"testmount":      testmount.TestMount{},
		"script":         scripts.Script{},
	}

	/* End Changeable Vars */
	Dryrun        bool
	logger        *logrus.Entry
	cmdLogger     *logrus.Entry
	strucElements = map[string]string{
		// All valid Keys in the config File. Map for easier iteration.
		"name": "", "tasks": "", "audit": "", "commands": "", "cmd": "", "properties": "", "load": "", "testFor": "", "result": "", "logging": "", "success": "", "useResult": "", "testProp": "",
	}
	infoElements = map[string]string{
		// All dependent keys. The key has the info, the value will be executed
		"name": "tasks", "audit": "commands", "testFor": "result", "testProp": "tasks",
	}
)

/* SetConf sets the path from the config file and imports everything
into Viper. If there is an Error, there program will stop!*/
func SetConf(config string) {
	logger = logging.MinimalLogger()

	viper.AddConfigPath(".")
	viper.SetConfigType("yaml")

	if config == "" {
		viper.SetConfigFile(standardConfig)
	} else {
		viper.SetConfigFile(config)
	}
	if err := viper.ReadInConfig(); err != nil {
		logging.PanicLogger(err)
	}

	logger.Debug("Config set.")
}

func init() {
	logger = logging.MinimalLogger()
}

/* Parser is the main element of this package. It runs the functions, that set
the global meta variables and starts the recursion loop.
*/
func Parser() {
	readMeta()                                       // Read all the Meta infos and store them in global variables
	oschecker.OsCompare(output.Data.OperatingSystem) //compare the given os with the real os
	output.SetOutputName(output.Data.Customer)

	// Read all the Scripts and store them in a global map
	readScripts()

	// Viper reads the block in the config file beginning by "Modules" and stores that in a map of interfaces
	names := make(map[string]string)
	modules := viper.Get("Modules")
	if modules == nil {
		logger.Panic(
			"'Modules' not Available. Please check Syntax!")
	}

	// Starting the RecursionLoop with the empty map and the content of the config
	logger.Debug("Recloop started.")
	recLoop(names, modules.([]interface{}), true)

	// Finalize the output
	output.FinalizeDebug(Dryrun)
	output.CreateJSONLog()
	output.CreateZipArchive()
}

/* readMeta sets the global variables for e.g. OS or customer */
func readMeta() {
	if viper.Get("OS") != nil {
		output.Data.OperatingSystem = viper.Get("OS").(string)
	}

	if viper.Get("Customer") != nil {
		output.Data.Customer = viper.Get("Customer").(string)
	}

	logger.Debug("Read Meta.")
}

/* readScripts takes all the scripts and stores them in a map.
The key is the name of the script and the value the content.*/
func readScripts() {
	scr := viper.Get("Scripts")

	if scr == nil {
		logger.Println("No Scripts found.")
	} else {
		scripts.Scripts = scr.(string)
	}

	logger.Debug("Read Scripts.")
}

/* recLoop goes through the input and stores the infos.
When it comes to the functions, these are executed.
Returns, whether the command was successfull (see --> execFuncs()).
*/
func recLoop(names map[string]string, inter []interface{}, toContinue bool) bool {
	for i := range inter {
		temp := inter[i].(map[interface{}]interface{}) // Jump in the next Layer.
		keys := make([]string, 0, len(temp))
		for k := range temp {
			keys = append(keys, k.(string))
		}
		sort.Slice(keys, func(i, j int) bool {
			return keys[i] > keys[j]
		})
		for _, k := range keys {
			if VerifyStrucs(temp, k) { //Syntax-Check!
				if k == "commands" {
					scripts.ScriptableResults = make([]interface{}, 0)
					// If the syntax is incorrect, the next audit should be executed nevertheless
					toContinue = true
				}
				if temp["testProp"] != nil { // !nil if there is a testProp in the map
					testMap, ok := temp["testProp"].(map[interface{}]interface{}) // get the elements of testprop
					if verifyFuncs(names["testFor"], testMap, make(map[string]string)) && ok {
						art, works := execFuncs(names, names["testFor"], commands.Cmd{}, testMap)
						if !works {
							logger.Infof("Switch '%s' in Module '%s' not executed.", testMap, art.ModulName)

							//Delete all commands in temp, to make sure they don't get executed.
							delete(temp["tasks"].([]interface{})[0].(map[interface{}]interface{}), "commands")
						} else {
							logger.Debugf("Switch '%s' in Module '%s' executed.", testMap, art.ModulName)
						}

						// Delete the key, so it wont be called in the next iteration
						delete(temp, "testProp")
					} else {
						logger.Error("Syntax-Error at testProp!")
					}
				}

				if tempVar, ok := temp[k].(string); ok { // Create Map with all Infos for logging
					names[k] = tempVar
				}
				_, ok := infoElements[k]
				if (temp["load"] != nil) || ok { // Should something be loaded?
					if temp["load"] != nil { // Should something be loaded?
						if k == "load" { // Only call load at "load" and not at "audit", "module" etc
							toContinue = loadFile(temp[k].(string), names, toContinue)
							delete(names, "load")
						}
					} else if ok && temp[infoElements[k]] != nil { // If the Element has Information
						toContinue = recLoop(names, temp[infoElements[k]].([]interface{}), toContinue)
					} else if names["testFor"] == "" {
						logger.Errorf("Syntax-Problem at Modul '%s' in the audit '%s' \n", names["name"], names["audit"])
					}
				} else if k == "properties" {
					toContinue = verifyAndCallFuncs(temp, toContinue, names)
				}
			}
		}
	}
	return toContinue
}

/* VerifyStrucs checks, whether the given struct is implemented or not.
 */
func VerifyStrucs(temp map[interface{}]interface{}, s string) bool {

	// Is this a known key?
	_, ok := strucElements[s]
	if !ok {
		logger.Errorf("The key %s is not known. Please check syntax!", s)
		return false
	}

	// Has the key valid content?
	if temp[s] == nil {
		logger.Errorf("There is a value missing at %s.\n", s)
		return false
	}
	return true
}

// verifyAndCallFuncs tries to verify the functions and calls them if verified.
func verifyAndCallFuncs(temp map[interface{}]interface{}, toContinue bool, names map[string]string) bool {

	// The actual name of the command (e.g. "grep")
	commandName := temp["cmd"].(string)
	commandName = strings.ToLower(commandName)

	// All the properties are put in a map, so they can be evaluated in the execute functions.
	propertiesMap := temp["properties"].(map[interface{}]interface{})
	var cmd commands.Cmd

	// Verify tha all properties are named correctly.
	if cmd.Verify(temp, names) {
		var res bool

		// CMD struct for every command
		res, cmd = cmd.Create(temp, names)
		if !res {
			logger.Errorf("There was an error creating the cmd-struct in %s", names["audit"])
			return false
		}
	}
	if verifyFuncs(commandName, propertiesMap, names) && toContinue { // Are all the properties correctly named and did the previous command run successful?
		_, toContinue = execFuncs(names, commandName, cmd, propertiesMap) // Now start the functions!
	} else {
		logger.Debugf("%s not executed. There was an error in the command before.", commandName)
	}
	return toContinue
}

/* verifyFuncs simply checks, whether the properties given by the config really exists.
If there is a spelling error or something like that, the program continues, but it will be logged.
*/
func verifyFuncs(command string, m map[interface{}]interface{}, names map[string]string) bool {

	// Get the command from the command Map and verify it.
	if commandMap[command] != nil {
		return commandMap[command].Verify(m, names)
	} else {
		logger.Errorf("'%s' not yet implemented or misspelled.", command)
		return false
	}
}

/* execFuncs executes the commands and creates a Artifact. The artifact is given to output and then
be logged. Returns, whether the command was successfull.
*/
func execFuncs(names map[string]string, command string, cmd commands.Cmd, m map[interface{}]interface{}) (commands.Artifact, bool) {
	com, ok := commandMap[strings.ToLower(command)]
	if !ok {
		logger.Errorf("'%s' not yet implemented or misspelled.", command)
		return commands.Artifact{}, false
	}
	if m == nil || m[""] != nil || len(m) == 0 {
		logger.Errorf("Map in command '%s' empty.", command)
		return commands.Artifact{}, false
	}

	// Create the struct with the given names
	ok, struc := com.Create(cmd, m, names)
	if !ok {
		logger.Errorf("The command '%s' could not be created.", command)
		return commands.Artifact{}, true
	}

	if Dryrun {
		return commands.Artifact{}, true
	}
	logger.Debugf("Execute %s.", command)
	if len(names) != 0 {
		cmdLogger = logging.CMDLineLogger(names)
		cmdLogger.Infof("Execute %s.", command)
	}

	art, toContinue := struc.Execute(names) // And then execute it
	if !toContinue {
		logger.Debugf("%s failed.", command)
	} else {
		logger.Debugf("%s succeeded.", command)
	}

	// Log the artifact
	if art.TaskName != "" {
		output.LogArtifact(art, toContinue, false, command) // Log the artifact
	}
	scripts.ScriptableResults = append(scripts.ScriptableResults, art.Result)

	// Return, whether the command was successfull
	return art, toContinue

}
