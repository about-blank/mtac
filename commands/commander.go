package commands

import (
	"github.com/fatih/structs"
	"github.com/sirupsen/logrus"
)

// type Commander is the interface for all structs with the given three functions.
type Commander interface {
	Create(cmd Cmd, m map[interface{}]interface{}, names map[string]string) (bool, Commander)
	Execute(names map[string]string) (Artifact, bool)
	Verify(m map[interface{}]interface{}, names map[string]string) bool
}

// Contains checks, if a string is in another slice of strings
func Contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

// VerifyStruc checks the syntactic correctness of a struct with the commander interface.
func VerifyStruc(com Commander, m map[interface{}]interface{}, logger *logrus.Entry) bool {

	if len(m) == 0 {
		logger.Warn("Empty CMD given.")
		return true
	} else {
		for k := range m {
			if _, ok := k.(string); !ok || k == nil {
				logger.Errorf("Problems parsing '%s'.", k)
				return false
			}
			if !Contains(structs.Names(com), k.(string)) {
				logger.Errorf("'%s' not known in the command. Did you misspell it?\n", k.(string)) //Error-Logging!
				return false
			}
		}
		return true
	}
}

// InitCmdScript creates an empty cmd script.
func InitCmdScript() Cmd {
	return Cmd{}
}

// type Artifact has all the information for logging.
type Artifact struct {
	ModulName string
	TaskName  string
	IsFile    bool
	Result    string
	Filepath  string
}
