// Package commands is the parent-package to all commands. It implements the Commander-Interface and the Artifact struct.
package commands

import (
	"mtac/logging"

	"github.com/fatih/structs"
)

/* Struct with multiple parameters, that can be used with the Create function.
 * Properties is only used for syntactic reasons. If it's missing, Verify fails
 */
type Cmd struct {
	cmd        string
	properties string
}

// Create is the function, that initializes the struct and checks the syntax.
func (c Cmd) Create(m map[interface{}]interface{}, names map[string]string) (bool, Cmd) {
	logger := logging.CommandLogger(names)
	struc := Cmd{}
	struc.properties = ""
	if temp, ok := m["cmd"].(string); !ok {
		logger.Error("There was an Error creating the cmd-struct! cmd misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Error("No command given")
		struc.cmd = temp
		return false, struc
	} else {
		struc.cmd = temp
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (c Cmd) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	if len(m) == 0 {
		logger.Warn("Empty CMD given.")
		return true
	} else {
		for k := range m {
			if _, ok := k.(string); !ok || k == nil {
				return false
			}
			if !Contains(structs.Names(c), k.(string)) {
				logger.Errorf("'%s' not known in the command. Did you misspell it?\n", k.(string))
				return false
			}
		}
		return true
	}
}
