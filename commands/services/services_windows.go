package services

import (
	"mtac/commands"
	"mtac/commands/grep"
	"mtac/logging"
	"strings"

	ps "github.com/bhendo/go-powershell"
	"github.com/bhendo/go-powershell/backend"
	"github.com/sirupsen/logrus"
)

var services string

// Safes all services in the services string
func setServices(logger *logrus.Entry) {
	back := &backend.Local{}

	// start a local powershell process
	shell, err := ps.New(back)
	if err != nil {
		logger.Error(err)
	}
	defer shell.Exit()

	serv, _, err := shell.Execute("Get-Service")
	if err != nil {
		logger.Error(err)
	}
	services = serv
}

// Execute receives a services-struct and checks its boolean statements by testing if the given service is enabled  and/or running. If both conditions are contextual true, it returns 'true'.
func (s Services) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)

	// Checks if services list is already set or not
	if services == "" {
		setServices(logger)
	}

	// Greps the specific service
	line := grep.GrepTextInString(string(services), false, s.service, names)
	logger.Debug("Found this line in services: ", line)

	// Checks if the given service is running or not
	var result string
	var resultBool bool
	if line != "" && strings.Contains(line, "Running") {
		result = s.service + " is running"
		resultBool = true
	} else {
		if line != "" && strings.Contains(line, "Stopped") {
			result = s.service + " is disabled"
			logger.Warn(result)
			resultBool = false
		} else {
			result = s.service + " not exists"
			logger.Warn(result)
			resultBool = false
		}
	}

	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    result,
		Filepath:  "",
	}

	if resultBool != s.running {
		logger.Warnf("%s running should be %t, but is %t.", s.service, s.running, resultBool)
	}

	return a, (resultBool == s.running)
}
