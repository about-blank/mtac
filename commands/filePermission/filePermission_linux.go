package filePermission

import (
	"mtac/commands"
	"mtac/logging"
	"os"
	"os/user"
	"strconv"
	"syscall"
)

// Execute is the function to execute the command. It checks, who as access to a file.
func (f FilePermission) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	logger.Debug("Execute FilePermission.")
	info, err := os.Lstat(f.fileName)
	var result string

	if err == nil {
		fileMode := info.Mode()
		permissions := fileMode.String()
		var owner string
		var group string
		var uid int
		var gid int

		if stat, ok := info.Sys().(*syscall.Stat_t); ok {
			uid = int(stat.Uid)
			gid = int(stat.Gid)

			//we now have to convert the UINT information into a string so that we can look up the user and the group
			u := strconv.FormatUint(uint64(uid), 10)
			g := strconv.FormatUint(uint64(gid), 10)

			//now we are looking up the user and the group and save them in the before declared strings
			if user_temp, user_err := user.LookupId(u); user_err == nil {
				owner = user_temp.Username
			} else {
				owner = "Something went wrong, looking up the owner: " + user_err.Error()
			}

			if group_temp, group_err := user.LookupGroupId(g); group_err == nil {
				group = group_temp.Name
			} else {
				group = "Something went wrong, looking up the group of the owner: " + group_err.Error()
			}

		}

		result = ("Permissions:" + permissions + ", Owner:" + owner + ", Group:" + group)

		a := commands.Artifact{
			ModulName: names["name"],
			TaskName:  names["audit"],
			IsFile:    false,
			Result:    result,
			Filepath:  "",
		}

		return a, true
	} else {
		result = "Something went wrong:\n" + err.Error()
		a := commands.Artifact{
			ModulName: names["name"],
			TaskName:  names["audit"],
			IsFile:    false,
			Result:    result,
			Filepath:  "",
		}
		logger.Error(info, err.Error())
		return a, false
	}

}

func InitScriptFilePerm(file string) *FilePermission {
	return &FilePermission{
		cmdBase:  commands.InitCmdScript(),
		fileName: file,
	}
}
