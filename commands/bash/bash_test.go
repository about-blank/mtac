package bash

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// General tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"input": "", "expectedResult": ""}, Res: false},

	// input tests
	{M: map[interface{}]interface{}{"expectedResult": ""}, Res: false},
	{M: map[interface{}]interface{}{"input": true, "expectedResult": ""}, Res: false},
	{M: map[interface{}]interface{}{"input": "foo", "expectedResult": ""}, Res: true},

	// expectedResult
	{M: map[interface{}]interface{}{"input": "foo"}, Res: true},
	{M: map[interface{}]interface{}{"input": "foo", "expectedResult": ""}, Res: true},
	{M: map[interface{}]interface{}{"input": "foo", "expectedResult": true}, Res: false},
	{M: map[interface{}]interface{}{"input": "foo", "expectedResult": "bar"}, Res: true},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, Bash{})
}
