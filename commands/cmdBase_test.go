package commands

import "testing"

// Random inputs to verify funcs
var verifyArray = []map[interface{}]interface{}{
	{true: ""},
	{"foo": true},
	{"test": ""},
	{"": ""},
}

// TestStruc for create and Verify --> Is filled in the _test.go files
type TestStruc struct {
	M   map[interface{}]interface{}
	Res bool
}

// TestStruc for cmd
var resultsCMD = []TestStruc{
	// general tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"cmd": ""}, Res: false},
	{M: map[interface{}]interface{}{"cmd": "foo"}, Res: true},

	// cmd tests
	{M: map[interface{}]interface{}{"cmd": ""}, Res: false},
	{M: map[interface{}]interface{}{"cmd": true}, Res: false},
	{M: map[interface{}]interface{}{"cmd": "foo"}, Res: true},
}

var cmd = Cmd{}

// Test create function of cmd (not a commander, so not with the other)
func TestCmd_Create(t *testing.T) {
	for i, test := range resultsCMD {
		res, _ := cmd.Create(test.M, make(map[string]string))
		if res != test.Res {
			t.Errorf("Error at test %s. ID-number %v.\n", test.M, i)
		}
		res = cmd.Verify(test.M, make(map[string]string))
		if res == false {
			t.Errorf("Error at test %s. ID-number %v.\n", test.M, i)
		}
		for i, test := range verifyArray {
			// Second try some random input
			res := cmd.Verify(test, make(map[string]string))
			if res != false {
				t.Errorf("Error at test %s. ID-number %v.\n", test, i)
			}
		}
	}
}
