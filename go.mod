module mtac

go 1.16

require (
	github.com/bhendo/go-powershell v0.0.0-20190719160123-219e7fb4e41e
	github.com/dop251/goja v0.0.0-20210614154742-14a1ffa82844
	github.com/fatih/structs v1.1.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/juju/testing v0.0.0-20210324180055-18c50b0c2098 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.8.0
	github.com/zcalusic/sysinfo v0.0.0-20210609180555-aff387a52b3a
)
