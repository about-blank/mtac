// Package filePermission contains the create, verify and execute functions for the command.
package filePermission

import (
	"mtac/commands"
	"mtac/logging"
)

// Struct with multiple parameters, that can be used with the Create function.
type FilePermission struct {
	cmdBase  commands.Cmd
	fileName string
}

// Create is the function, that initializes the struct and checks the syntax.
func (f FilePermission) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	logger.Debug("Create FilePermission.")
	struc := FilePermission{cmdBase: cmd}
	if temp, ok := m["fileName"].(string); !ok {
		logger.Error("'FileName' misspelled or not available.")
		return false, struc
	} else if temp == "" {
		logger.Warn("'FileName' is empty.")
	} else {
		struc.fileName = temp
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (f FilePermission) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(f, m, logger)
}
