package scripts

import (
	"fmt"
	"mtac/commands"
	"mtac/commands/bash"
	"mtac/commands/filePermission"
	"mtac/commands/find"
	"mtac/commands/grep"
	isinstalled "mtac/commands/isInstalled"
	"mtac/commands/services"
	testmod "mtac/commands/testMod"
	"mtac/commands/testmount"
	"mtac/logging"
	"mtac/output"
	"os/exec"
	"strings"

	"github.com/dop251/goja"
)

// type Mtac is a struct for scripting.
type Mtac struct{}

/*
	Declares keys for every command map
	----------> DO NOT CHANGE <----------
*/
var (
	bashKeys      = []string{"input", "expectedResult"}
	fPKeys        = []string{"fileName"}
	findKeys      = []string{"dir"}
	grepKeys      = []string{"input", "target", "regex", "caseSensitive"}
	isInsKeys     = []string{"packageName"}
	serviceKeys   = []string{"service", "enabled", "running"}
	testModKeys   = []string{"module"}
	testMountKeys = []string{"dev", "regex", "isSet", "options"}

	emptyNamesMap = map[string]string{
		"ModuleName": "",
		"TaskName":   "",
	}
)

/*
	mapBuilder - Due to constrains which emerge from the way commands are implemented into mtac it is
	necessary to build an map consisting out of the keys expected by the "create"-function of each command
	and the values given by the corresponding function in the goja-runtime.
	This function achieves that by matching the previous declared key-arrays with the arguments from within in runtime.
*/
func mapBuilder(keys []string, args ...interface{}) map[interface{}]interface{} {
	results := make(map[interface{}]interface{})
	for i, key := range keys {
		if i < len(args) {
			results[key] = args[i]
		} else {
			results[key] = nil
		}
	}
	return results
}

/*
	setNamesMap fills the empty names map.
*/
func setNamesMap(names map[string]string) {
	emptyNamesMap["ModuleName"] = names["ModuleName"]
	emptyNamesMap["TaskName"] = names["audit"]
}

/*
	NewM returns a new mtac-object upon being called.
*/
func NewM(vm *goja.Runtime) *Mtac {
	return &Mtac{}
}

/*
	BashEvalB - All functions beneath this statement are function which can be called using the "mtac"-object inside the goja runtime.
	Since all of them function largely the same most of the explanatory comments will just reference "BashEvalB".

	Most functions return a string, those which return a boolean are marked with a trailing "B".
*/

func (m Mtac) BashEval(dev string, expectedResult string) bool {
	var cmd bash.Bash                                     // initialzing needed command-struct
	propMap := mapBuilder(bashKeys, dev, expectedResult)  // builds input-map for create function using params
	cmdMap := commands.InitCmdScript()                    // initializes cmdBase-struct needed by create function
	_, bash := cmd.Create(cmdMap, propMap, emptyNamesMap) // creates specified struct
	a, r := bash.Execute(emptyNamesMap)                   // executes given struct, since the return-type is a boolean,
	output.LogArtifact(a, r, true, "BashEval (Script)")
	return r // the status-bool provieded by "execute" is returned.
}

/*
	BashC - This is one of two function not calling a previous defined command, since there would be
	Problems concerning the test-artifact-formating.
	Executes a given command and returns its output as a string.
*/
func (m Mtac) BashC(cmd string) string {
	a := commands.Artifact{
		ModulName: emptyNamesMap["ModuleName"],
		TaskName:  emptyNamesMap["TaskName"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}

	logger := logging.SimpleLogger()
	bash := exec.Command("bash", "-c", cmd)
	out, err := bash.CombinedOutput()
	if err != nil {
		logger.Printf("Error while executing Bash-Command in Script, Error: %v", err)
	}
	out = []byte(strings.TrimSuffix(string(out), "\n"))
	a.Result = string(out)
	output.LogArtifact(a, true, true, "BashC (Script)")
	return a.Result
}

/*
	FilePermission works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) FilePermission(fileName string) string {
	var cmd filePermission.FilePermission
	propMap := mapBuilder(fPKeys, fileName)
	cmdMap := commands.InitCmdScript()
	_, fp := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := fp.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "File Permission (Script)")
	return a.Result
}

/*
	Find works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) Find(dir string) string {
	var cmd find.Find
	propMap := mapBuilder(findKeys, dir)
	cmdMap := commands.InitCmdScript()
	_, find := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := find.Execute(emptyNamesMap)
	if dir == strings.TrimSuffix(a.Result, "\n") {
		return ""
	}
	output.LogArtifact(a, r, true, "Find (Script)")
	return a.Result
}

/*
	GrepEString acts a as pass-through to a helper-function defined in grep.go.
*/
func (m Mtac) GrepString(input, target string) string {
	res := grep.GrepTextInString(target, true, input, make(map[string]string))
	a := commands.Artifact{
		ModulName: emptyNamesMap["ModuleName"],
		TaskName:  emptyNamesMap["TaskName"],
		IsFile:    false,
		Result:    res,
		Filepath:  "",
	}
	output.LogArtifact(a, true, true, "GrepString (Script)")
	return res
}

/*
	GrepEFile works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) GrepFile(input, target string, caseSensitive bool) string {
	var cmd grep.Grep
	propMap := mapBuilder(grepKeys, input, target, true, caseSensitive)
	cmdMap := commands.InitCmdScript()
	_, grep := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := grep.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "GrepFile (Script)")
	return a.Result
}

/*
	IsInstalledB works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) IsInstalled(packageName string) bool {
	var cmd isinstalled.IsInstalled
	propMap := mapBuilder(isInsKeys, packageName)
	cmdMap := commands.InitCmdScript()
	_, isIn := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := isIn.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "IsInstalled (Script)")
	return r
}

/*
	SystemdB works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) TestService(service string, enabled, running bool) bool {
	var cmd services.Services
	propMap := mapBuilder(serviceKeys, service, enabled, running)
	fmt.Println(propMap)
	cmdMap := commands.InitCmdScript()
	_, systemd := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := systemd.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "TestService (Script)")
	return r
}

/*
	TestModB works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) TestMod(module string) bool {
	var cmd testmod.TestMod
	propMap := mapBuilder(testModKeys, module)
	cmdMap := commands.InitCmdScript()
	_, testMount := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := testMount.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "TestMod (Script)")
	return r
}

/*
	TestMountB works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) TestMount(dev string, regex, isSet bool, options []interface{}) bool {
	var cmd testmount.TestMount
	propMap := mapBuilder(testMountKeys, dev, regex, isSet, options)
	cmdMap := commands.InitCmdScript()
	_, bash := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := bash.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "TestMount (Script)")
	return r
}

/*
	Log initialize the logger.
*/
func (m Mtac) Log(message string) {
	l := logging.SimpleLogger()
	l.Info(message)
}
