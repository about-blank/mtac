# The Builder can be used to create a config.yaml for MTAC.
# WARNING: It needs to be updated to match the existing version of mtac!
import yaml

def createProperties(command):
    command = command.lower()
    prop = {}
    if command == 'bash':
        prop['input'] = input("Input: ")
        prop["expectedResult"] = input("Expected result: ")
    elif command == "grep":
        prop["caseSensitive"] = input("CaseSensitive? ")
        prop["target"] = input("Target: ")
        prop["regex"] = input("Regex: ")
        prop["searchString"] = input("SearchString: ")
    elif command == "isinstalled":
        prop["packageName"] = input("packageName: ")
    elif command == "systemd":
        prop["enabled"] = input("Test enabled? ")
        prop["running"] = input("Test running? ")
        prop["service"] = input("ServiceName: ")
    elif command == "testmod":
        prop["module"] = input("Modulname: ")
    elif command == "testmount":
        prop["regex"] = input("Regex: ")
        prop["mounted"] = input("Mounted: ")
        opt = []
        while True:
            opt.append(input("Option: "))
            if input("Another one? (y/n): ") == "n":
                break
        prop["options"] = opt
    elif command == "script":
        prop["script"] = input("Gimme da script: ")
        prop["function"] = input("Functionname: ")
        prop["expectedResult"] = input("What should be the result? ")
    else:
        print('error')
        return None
    return prop

def createCommand():
    m = {}
    m['cmd'] = input("Command-Type (grep,testmount,...): ")
    prop = createProperties(m['cmd'])
    if prop == None:
        return None
    m['properties'] = prop
    return m

def createAudit():
    audit = {}
    audit['audit'] = input('Whats the Name of the Audit: ')
    commands = []
    while True:
        comm = createCommand()
        if comm != None:
            commands.append(comm)
        if input('Next command(y/n)? ') == "n":
            break
    audit["commands"] = commands
    return audit

def createTask():
    task = {}
    task['name'] = input('Whats the Name of the Task: ')
    audits = []
    while True:
        audits.append(createAudit())
        if input('New Audit-Step (y/n)? ') == "n":
            break
    task["tasks"] = audits
    return task

def main():
    dict_file = createTask()

    with open(dict_file['name']+".yaml", 'w') as file:
        documents = yaml.dump(dict_file, file)

if __name__ == '__main__':  
    main()
