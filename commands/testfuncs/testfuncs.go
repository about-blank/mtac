// Package testfuncs contains all the functions and a struct to test all the commands without any redundant code.
package testfuncs

import (
	"mtac/commands"
	"reflect"
	"testing"
)

// TestStruc for create and Verify --> Is filled in the _test.go files
type TestStruc struct {
	M   map[interface{}]interface{}
	Res bool
}

var names = map[string]string{
	"audit": "foo",
	"name":  "bar",
}

/* TestCreate loops through all the commands in the struct above.  There it checks every map from the commands and compares them with their result.
 */
func TestCreate(t *testing.T, ts []TestStruc, com commands.Commander) {

	for i, test := range ts { // Iterate through TestStruct
		res, _ := com.Create(commands.Cmd{}, test.M, names)
		if res != test.Res {
			t.Errorf("Error at test %s. ID-number %v.\n", test.M, i)
		}
	}
	testVerify(t, ts, com)
}

// Random inputs for verify funcs
var verifyArray = []map[interface{}]interface{}{
	{true: ""},
	{"foo": true},
	{"test": ""},
	{"": ""},
}

// Verifies all Commands with the command map and a random array
func testVerify(t *testing.T, ts []TestStruc, com commands.Commander) {
	// Firstly iterate through the Map from Create and check every key
	for i, test := range ts {
		res := com.Verify(test.M, make(map[string]string))
		if !res {
			t.Errorf("Error at test %s. ID-number %v. %s\n", test.M, i, com)
		}
	}

	// Try some random input
	for i, test := range verifyArray {
		res := com.Verify(test, make(map[string]string))
		if res {
			t.Errorf("Error at test %s. ID-number %v. The commander is %v.\n", test, i, reflect.TypeOf(com))
		}
	}

}
