//package oschecker contains all files to determine on which os the user is.
package oschecker

import (
	"errors"
	"mtac/logging"
	"os"
	"strings"
)

// GetOS checks on which operating system the program is executed and returns "Windows", "Unix" ot throws an error, if unknown.
func GetOS() string {
	logger := logging.SimpleLogger()
	nul := os.DevNull
	switch nul {
	case "NUL":
		return "Windows"
	case "/dev/null":
		return "Unix"
	default:
		logger.Error("Unknown OS.")
		return nul
	}
}

// OsCompare gets a string and compares it to the real operating system. If they do not match, they throw an error.
func OsCompare(s string) {
	osName := getOS()
	same := strings.EqualFold(osName, s)

	if !same {
		err := errors.New("The given OS '" + s + "' does not match the real OS '" + osName + "'.")
		logging.PanicLogger(err)
	}
}
