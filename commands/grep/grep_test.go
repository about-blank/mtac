package grep

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// general tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},

	// caseSensitiveTests
	{M: map[interface{}]interface{}{"input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "", "input": "/s", "target": "/etc/fstab", "regex": true}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": true, "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": false, "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": false, "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},

	// inputtests
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "", "target": "/etc/fstab", "regex": true}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "target": "/etc/fstab", "regex": true}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "foo", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "true", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": true, "target": "/etc/fstab", "regex": true}, Res: false},

	//targettests
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "", "regex": true}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": true, "regex": true}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "regex": true}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "foo", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "true", "regex": true}, Res: true},

	//regextests
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": true}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": false}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": "true"}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": "false"}, Res: true},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": ""}, Res: false},
	{M: map[interface{}]interface{}{"caseSensitive": "true", "input": "/s", "target": "/etc/fstab", "regex": "foo"}, Res: false},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, Grep{})
}
