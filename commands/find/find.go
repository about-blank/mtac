// Package find contains the create, verify and execute functions for the command.
package find

import (
	"io/fs"
	"mtac/commands"
	"mtac/logging"
	"path/filepath"
	"strings"
)

// Struct with multiple parameters, that can be used with the Create function.
type Find struct {
	cmdBase commands.Cmd
	dir     string
}

// Create is the function, that initializes the struct and checks the syntax.
func (f Find) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {

	var logger = logging.CommandLogger(names)
	struc := Find{cmdBase: cmd}

	if res, ok := m["dir"].(string); !ok {
		logger.Errorf("'%s' is no valid string.", res)
		return false, struc
	}
	if m["dir"] == nil {
		logger.Error("Missing keys.")
		return false, struc
	} else if m["dir"] == "" {
		logger.Error("'dir' is empty.")
		return false, struc
	} else {
		struc.dir = m["dir"].(string)
		return true, struc
	}
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (f Find) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(f, m, logger)
}

func (f Find) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}
	var sb strings.Builder
	err := filepath.WalkDir(f.dir,
		func(path string, d fs.DirEntry, err error) error {
			sb.WriteString(path + "\n")
			return nil
		})
	if strings.TrimSuffix(sb.String(), "\n") == f.dir {
		logger.Warnf("Specified directory could not be found.")
	}
	a.Result = sb.String()
	if err != nil {
		logger.Errorf("Error : %v.", err)
	}

	if a.Result != "" {
		return a, true
	} else {
		logger.Warnf("Result is empty.")
		return a, false
	}
}
