package oschecker

import (
	"strings"
	"testing"
)

func Test_GetOs_linux(t *testing.T) {
	if GetOS() != "Unix" {
		t.Fail()
	}
}

func Test_OSCompare_linux(t *testing.T) {
	if strings.EqualFold(GetOS(), "Linux") {
		OsCompare("Ubuntu")
	}
}

func Test_GetOs_windows(t *testing.T) {
	if strings.EqualFold(GetOS(), "Windows") {
		if GetOS() != "Windows" {
			t.Fail()
		}
	}
}

func Test_OSCompare_windows(t *testing.T) {
	if strings.EqualFold(GetOS(), "Windows") {
		OsCompare("Windows")
	}
}
