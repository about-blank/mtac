package parser

import (
	"bufio"
	"io"
	"mtac/commands/scripts"
	"os"
	"path"

	"github.com/spf13/viper"
)

/* loadFile loads an file from the path and starts the recLoop with it, if it is a yaml.
If JavaScript execute.
*/
func loadFile(paths string, names map[string]string, toContinue bool) bool {
	typ := path.Ext(paths)
	if typ == ".yaml" || typ == ".yml" {
		dir, file := path.Split(paths)
		viper.SetConfigName(file)
		viper.AddConfigPath(dir)
		viper.SetConfigFile(paths)

		// Find and read the config file
		err := viper.ReadInConfig()
		if err != nil {
			logger.Errorf("Error load. File: %s. %s", paths, err)
			return false
		}
		for i := range viper.AllKeys() {
			vip := viper.Get(string(viper.AllKeys()[i]))
			if _, ok := vip.([]interface{}); !ok {
				logger.Errorf("Can't load %s into config.", paths)
				return false
			} else {
				logger.Debug("Loading file successfully.")
				return recLoop(names, vip.([]interface{}), toContinue)
			}

		}
		return true
	} else if typ == ".js" {
		jsstring := lineByLine(paths) + "\n"
		scripts.Scripts += jsstring
	}
	return true
}

/* lineByLine reads a file lineByLine*/
func lineByLine(file string) string {
	var result string
	var err error
	fd, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	defer fd.Close()

	reader := bufio.NewReader(fd)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			logger.Errorf("Can't read file. %v", err)
			break
		}
		result = result + line
	}
	return result + "}"
}
