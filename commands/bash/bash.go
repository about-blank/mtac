// Package bash contains the create, verify and execute functions for the command.
package bash

import (
	"mtac/commands"
	"mtac/logging"
)

// Struct with multiple parameters, that can be used with the Create function.
type Bash struct {
	cmdBase        commands.Cmd
	input          string
	expectedResult string
}

// Create is the function, that initializes the struct and checks the syntax.
func (b Bash) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := Bash{cmdBase: cmd}
	if temp, ok := m["input"].(string); !ok {
		logger.Error("'Input' misspelled or not availabe.")
		return false, struc
	} else if temp == "" {
		logger.Error("'Input' is empty.")
		struc.input = temp
		return false, struc
	} else {
		struc.input = temp
	}
	if m["expectedResult"] != nil {
		if temp, ok := m["expectedResult"].(string); !ok {
			logger.Error("'ExpectedResult' misspelled or not available.")
			return false, struc
		} else {
			struc.expectedResult = temp
		}
	} else {
		// resolves null problem with empty string. The user gives a null input, so he does not want to give any expected result
		struc.expectedResult = "z1rPLHTH1UJLkYO"
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (b Bash) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(b, m, logger)
}
