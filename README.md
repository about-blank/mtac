# MTAC
MTAC is a program for an automated operation system audit developed by [about:blank](https://www.about-blank.space/).  
It is written for Windows (7,8,10) and Linux (most distributions).  
To use it, you only need a config file and get a zipped folder as result.

## Usage  
After building mtac with `go build` you can use the program.  
  
Run: `./mtac -params`  

It has multiple parameters:
```   
  -c, --config string     specifies the path to the config-file
  -d, --dryrun            Dryrun. Only checks the syntax of the config file and does not execute any command.
  -h, --help              help for mtac
  -l, --loglevel string   specifies the log level (e.g. Info)
  -o, --output string     specifies the output-path
``` 

If there is no config or output given, mtac searches for the _mtac.yaml_ in the same path and saves the zip archive in the working directory.  

## Config
The config file _mtac.yaml_ can only be parsed as .yaml. Other extensions are not allowed. For more information about the config visit our [Wiki](https://gitlab.com/about-blank/mtac/-/wikis/home). 
A minimal example:  
  
```
OS: Ubuntu
Customer: about-blank
Modules:
  - name: example module
    tasks:
      - audit: example audit
        commands:
          - cmd: bash
            properties: 
              input: pwd
```

## Output  
The result of mtac is a zipped folder with the name and timestamp of execution.  
It contains multiple files:  
- debug.log: All infos, warnings and errors that were thrown during the execution.  
Example:  
```  
MTAC executed. Execution time:69.994853ms. 
420 commands executed. 
420 commands were successful 
0 commands failed 
--------------------------------------------------------------- 
Failed Audits: 
--------------------------------------------------------------- 
Logs: 
```
- output.json: Contains a machine-readable version of the debug information.
Example:  
```  
{
 "OperatingSystem": "Ubuntu",
 "Customer": "about:blank",
 "Modules": [
  {
   "Name": "example module",
   "Audits": [
    {
     "Name": "example audit",
     "Tasks": [
      {
       "Command": "bash",
       "ArtifactPath": "",
       "ResultPath": "mtac_about:blank_2021-06-09 T16-08/tests ubuntu/testbash/Outputs/8ef108bf-8726-42c1-8a1d-12560b20f10e.txt",
       "Success": true
      }
     ]
    }
   ]
  }
 ]
}
```  
- multiple folders: All the test artifacts, that were created during the execution.  
They have random generated names, and can be found either with help of the debug.log or the infos from the output.json

## Contributing
Contributions are welcome. For major changes, please open an issue first to discuss what you would like to change.
Feel free to check out our [documentation](https://www.about-blank.space/mtac/doc/).

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)