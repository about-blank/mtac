package reg

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	// General tests
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"path": "", "key": ""}, Res: false},

	// path tests
	{M: map[interface{}]interface{}{"path": "", "key": "abc"}, Res: false},
	{M: map[interface{}]interface{}{"path": true, "key": "abc"}, Res: false},
	{M: map[interface{}]interface{}{"path": false, "key": "abc"}, Res: false},
	{M: map[interface{}]interface{}{"key": "abc"}, Res: false},
	{M: map[interface{}]interface{}{"path": "abc", "key": "abc"}, Res: true},

	// key tests
	{M: map[interface{}]interface{}{"path": "foo", "key": "abc"}, Res: true},
	{M: map[interface{}]interface{}{"path": "foo", "key": ""}, Res: false},
	{M: map[interface{}]interface{}{"path": "foo"}, Res: false},
	{M: map[interface{}]interface{}{"path": "foo", "key": true}, Res: false},
	{M: map[interface{}]interface{}{"path": "foo", "key": false}, Res: false},
	{M: map[interface{}]interface{}{"path": "foo", "key": "true"}, Res: true},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, Registry{})
}
