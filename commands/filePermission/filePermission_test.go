package filePermission

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"fileName": ""}, Res: true},
	{M: map[interface{}]interface{}{"fileName": "foo"}, Res: true},
	{M: map[interface{}]interface{}{"fileName": true}, Res: false},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, FilePermission{})
}
