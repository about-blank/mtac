package testMod

import (
	"mtac/commands/testfuncs"
	"testing"
)

var testStruc = []testfuncs.TestStruc{
	{M: map[interface{}]interface{}{}, Res: false},
	{M: map[interface{}]interface{}{"module": true}, Res: false},
	{M: map[interface{}]interface{}{"module": "foo"}, Res: true},
	{M: map[interface{}]interface{}{"module": ""}, Res: false},
}

func TestCreate(t *testing.T) {
	testfuncs.TestCreate(t, testStruc, TestMod{})
}
