// Package grep contains the create, verify and execute functions for the command.
package grep

import (
	"bufio"
	"mtac/commands"
	"mtac/logging"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Struct with multiple parameters, that can be used with the Create function.
type Grep struct {
	cmdBase       commands.Cmd
	caseSensitive bool
	input         string
	regex         bool
	target        string
}

// Create is the function, that initializes the struct and checks the syntax.
func (g Grep) Create(cmd commands.Cmd, m map[interface{}]interface{}, names map[string]string) (bool, commands.Commander) {
	var logger = logging.CommandLogger(names)
	struc := Grep{cmdBase: cmd}
	if temp := m["caseSensitive"]; temp != nil {
		if res, ok := temp.(bool); !ok {
			cs, err := strconv.ParseBool(temp.(string))
			if err != nil {
				logger.Errorf("'%s' is no valid boolean.\n", temp)
				return false, struc
			}
			struc.caseSensitive = cs
		} else {
			struc.caseSensitive = res
		}
	}
	if m["input"] == nil {
		logger.Error("'Input' is emtpy.")
		return false, struc
	} else {
		if temp, ok := m["input"].(string); !ok {
			logger.Error("'input' misspelled or not available.")
			return false, struc
		} else if temp == "" {
			logger.Warn("'input' is empty.")
			return false, struc
		} else {
			struc.input = temp
		}
	}
	if m["target"] == nil {
		logger.Warn("'target' is empty.")
		return false, struc
	} else {
		if temp, ok := m["target"].(string); !ok {
			logger.Error("'Target' misspelled or not available.")
			return false, struc
		} else if temp == "" {
			logger.Warn("Target is empty.")
			return false, struc
		} else {
			struc.target = temp
		}
	}
	if temp := m["regex"]; temp != nil {
		if res, ok := temp.(bool); !ok {
			cs, err := strconv.ParseBool(temp.(string))
			if err != nil {
				logger.Errorf("'%s' is no valid boolean.\n", temp)
				return false, struc
			}
			struc.regex = cs
		} else {
			struc.regex = res
		}
	}
	return true, struc
}

// Verify - Calls the verify function in cmdBase with the map given and the Commander on which it is executed.
func (g Grep) Verify(m map[interface{}]interface{}, names map[string]string) bool {
	logger := logging.CommandLogger(names)
	return commands.VerifyStruc(g, m, logger)
}

/*
Execute - By reading the binary input of a file through bufio.Scanner() this function iterates
over each line of to given input and appends lines with occurrences of either g.regex or
g.searchstring to the result-string.
*/
func (g Grep) Execute(names map[string]string) (commands.Artifact, bool) {
	var logger = logging.CommandLogger(names)
	a := commands.Artifact{
		ModulName: names["name"],
		TaskName:  names["audit"],
		IsFile:    true,
		Result:    "",
		Filepath:  g.target,
	}

	var result string
	var textToSearch string
	if g.regex {
		textToSearch = g.input
	} else {
		textToSearch = `.*` + g.input + `.*`
	}
	if g.caseSensitive {
		// prefixing (?i) to a regex to make it caseINsensitive
		textToSearch = `(?i)` + textToSearch
	}
	file, _ := os.Open(g.target)

	defer file.Close()
	scanner := bufio.NewScanner(file)
	rmatch := regexp.MustCompile(textToSearch)
	for scanner.Scan() {
		// scanner.Text uses line break as delimiter for input-stream.
		line := scanner.Text() + "\n"
		if rmatch.FindStringSubmatch(line) != nil {
			result += line
		}
	}
	if err := scanner.Err(); err != nil {
		logger.Debug(err.Error())
	}
	a.Result = strings.TrimSpace(result)
	if result != "" {
		return a, true
	} else {
		return a, false
	}
}

/*
GrepTextinString searches through a string in the same fashion as described for execute().
*/
func GrepTextInString(stringToSearch string, isR bool, match string, names map[string]string) string {
	var logger = logging.CommandLogger(names)
	var result string
	scanner := bufio.NewScanner(strings.NewReader(stringToSearch))
	if !isR {
		match = `.*` + match + `.*`
	}
	rmatch := regexp.MustCompile(match)
	for scanner.Scan() {
		line := scanner.Text() + "\n"
		if rmatch.FindStringSubmatch(line) != nil {
			result += line
		}
	}
	if err := scanner.Err(); err != nil {
		logger.Error(err)
	}
	return strings.TrimSpace(result)
}
