package scripts

import (
	"mtac/commands"
	"mtac/commands/bash"
	"mtac/commands/filePermission"
	"mtac/commands/find"
	"mtac/commands/gpo"
	"mtac/commands/grep"
	isinstalled "mtac/commands/isInstalled"
	"mtac/commands/reg"
	"mtac/commands/services"
	"mtac/logging"
	"mtac/output"
	"os/exec"
	"strings"

	"github.com/dop251/goja"
)

// type Mtac is a struct for scripting.
type Mtac struct{}

/*
	Declares keys for every command map
	----------> DO NOT CHANGE <----------
*/
var (
	bashKeys    = []string{"input", "expectedResult"}
	fPKeys      = []string{"fileName"}
	findKeys    = []string{"dir"}
	grepKeys    = []string{"input", "target", "regex", "caseSensitive"}
	isInsKeys   = []string{"packageName"}
	gpoKeys     = []string{"name", "isApplied"}
	regKeys     = []string{"regKey", "param"}
	serviceKeys = []string{"service", "enabled", "running"}

	emptyNamesMap = map[string]string{
		"name":  "",
		"audit": "",
	}
)

/*
	mapBuilder - Due to constrains which emerge from the way commands are implemented into mtac it is
	necessary to build an map consisting out of the keys expected by the "create"-function of each command
	and the values given by the corresponding function in the goja-runtime.
	This function achieves that by matching the previous declared key-arrays with the arguments from within in runtime.
*/
func mapBuilder(keys []string, args ...interface{}) map[interface{}]interface{} {
	results := make(map[interface{}]interface{})
	for i, key := range keys {
		if i < len(args) {
			results[key] = args[i]
		} else {
			results[key] = nil
		}
	}
	return results
}

/*
	NewM returns a new mtac-object upon being called.
*/
func NewM(vm *goja.Runtime) *Mtac {
	return &Mtac{}
}

/*
	BashEvalB - All functions beneath this statement are function which can be called using the "mtac"-object inside the goja runtime.
	Since all of them function largely the same most of the explanatory comments will just reference "BashEvalB".

	Most functions return a string, those which return a boolean are marked with a trailing "B".
*/

func (m Mtac) BashEval(dev string, expectedResult string) bool {
	// Initialize and build struct
	var cmd bash.Bash
	propMap := mapBuilder(bashKeys, dev, expectedResult)
	cmdMap := commands.InitCmdScript()

	// Create and execute struct
	_, bash := cmd.Create(cmdMap, propMap, emptyNamesMap)
	_, r := bash.Execute(emptyNamesMap)
	return r
}

/*
	BashC - This is one of two function not calling a previous defined command, since there would be
	Problems concerning the test-artifact-formating.
	Executes a given command and returns its output as a string.
*/
func (m Mtac) BashC(cmd string) string {
	a := commands.Artifact{
		ModulName: emptyNamesMap["ModuleName"],
		TaskName:  emptyNamesMap["TaskName"],
		IsFile:    false,
		Result:    "",
		Filepath:  "",
	}

	bash := exec.Command("bash", "-c", cmd)
	out, err := bash.CombinedOutput()
	if err != nil {
		l := logging.SimpleLogger()
		l.Info("Error while executing Bash-Command in Script, Error: %v", err)
	}
	out = []byte(strings.TrimSuffix(string(out), "\n"))

	output.LogArtifact(a, true, true, "BashC (Script)")
	return string(out)
}

/*
	FilePermission works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) FilePermission(fileName string) string {
	var cmd filePermission.FilePermission
	propMap := mapBuilder(fPKeys, fileName)
	cmdMap := commands.InitCmdScript()
	_, fp := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := fp.Execute(emptyNamesMap)

	output.LogArtifact(a, r, true, "File Permission (Script)")
	return a.Result
}

/*
	Find works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) Find(dir string) string {
	var cmd find.Find
	propMap := mapBuilder(findKeys, dir)
	cmdMap := commands.InitCmdScript()
	_, find := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := find.Execute(emptyNamesMap)
	if dir == strings.TrimSuffix(a.Result, "\n") {
		return ""
	}
	output.LogArtifact(a, r, true, "Find (Script)")
	return a.Result
}

/*
	GrepEString acts a as pass-through to a helper-function defined in grep.go.
*/
func (m Mtac) GrepString(input, target string) string {
	res := grep.GrepTextInString(target, true, input, emptyNamesMap)
	a := commands.Artifact{
		ModulName: emptyNamesMap["ModuleName"],
		TaskName:  emptyNamesMap["TaskName"],
		IsFile:    false,
		Result:    res,
		Filepath:  "",
	}
	output.LogArtifact(a, true, true, "GrepString (Script)")
	return res
}

/*
	GrepEFile works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) GrepFile(input, target string, caseSensitive bool) string {
	var cmd grep.Grep
	propMap := mapBuilder(grepKeys, input, target, true, caseSensitive)
	cmdMap := commands.InitCmdScript()
	_, grep := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := grep.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "GrepFile (Script)")
	return a.Result
}

/*
	IsInstalledB works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) IsInstalled(packageName string) bool {
	var cmd isinstalled.IsInstalled
	propMap := mapBuilder(isInsKeys, packageName)
	cmdMap := commands.InitCmdScript()
	_, isIn := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := isIn.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "IsInstalled (Script)")
	return r
}

/*
	Service works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) Service(service string, enabled bool, running bool) bool {
	var cmd services.Services
	propMap := mapBuilder(serviceKeys, service, enabled, running)
	cmdMap := commands.InitCmdScript()
	_, serv := cmd.Create(cmdMap, propMap, emptyNamesMap)
	a, r := serv.Execute(emptyNamesMap)
	output.LogArtifact(a, r, true, "TestService (Script)")
	return r
}

/*
	GPO works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) Gpo(name string, isApplied bool) bool {
	var cmd gpo.GPO
	propMap := mapBuilder(gpoKeys, name, isApplied)
	cmdMap := commands.InitCmdScript()
	_, g := cmd.Create(cmdMap, propMap, emptyNamesMap)
	_, r := g.Execute(emptyNamesMap)
	return r
}

/*
	Reg works analog to BashEvalB. For a detailed explanation see BashEvalB()
*/
func (m Mtac) Reg(regKey, param string) string {
	var cmd reg.Registry
	propMap := mapBuilder(regKeys, regKey, param)
	cmdMap := commands.InitCmdScript()
	_, r := cmd.Create(cmdMap, propMap, emptyNamesMap)
	res, _ := r.Execute(emptyNamesMap)
	return res.Result
}

/*
	setNamesMap fills the empty names map.
*/
func setNamesMap(names map[string]string) {
	emptyNamesMap["ModuleName"] = names["ModuleName"]
	emptyNamesMap["TaskName"] = names["audit"]
}

/*
	Log initialize the logger.
*/
func (m Mtac) Log(message string) {
	l := logging.SimpleLogger()
	l.Info(message)
}
